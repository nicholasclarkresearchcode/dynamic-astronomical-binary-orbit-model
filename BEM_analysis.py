# -*- coding: utf-8 -*-
"""
Created on Mon May  3 20:42:25 2021

@author: nickj
"""

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import BEM_analysis_functions as bem
import os
import time as time
start = time.time()

#initial constants. Working with units of years, Msun, and Rsun
Rsun = 6.96e8 #metres, radius of the sun
Msun = 1.989e30#kg, mass of the sun
yr = 3.1536e7 #seconds in a year
k = 0.2 #const ranging from 0.1 to 0.01, taking the same value as Prof Neilson in 2015, which is 0.2 for some reason?
G = 6.67408e-11*Msun*yr**2/(Rsun**3) #gravitational constant G



'''
What to do on the next run
'''
do_phase1 = 0 #run phase 1 binary evolution model

do_phase2 = 0 #run phase 2 binary evolution model

do_phase3 = 0 #run phase 3 binary evolution model

do_phase4 = 0 #run phase 4 binary evolution model

do_phase5 = 0 #run phase 5 binary evolution model

do_phase6 = 0 #run phase 6 binary evolution model

do_phase7 = 0 #run phase 7 binary evolution model



'''
Making Directories and Paths
'''
#phase 1
try:
    os.mkdir('phase_1')
except:
    pass

path1_plots = Path('phase_1/')

try:
    os.mkdir(os.path.join(path1_plots, 'Data'))
except:
    pass

path1 = Path('phase_1/Data/')



#phase 2
try:
    os.mkdir('phase_2')
except:
    pass

path2_plots = Path('phase_2/')

try:
    os.mkdir(os.path.join(path2_plots, 'Data'))
except:
    pass

path2 = Path('phase_2/Data/')



#phase 3
try:
    os.mkdir('phase_3')
except:
    pass

path3_plots = Path('phase_3/')

try:
    os.mkdir(os.path.join(path3_plots, 'Data'))
except:
    pass

path3 = Path('phase_3/Data/')



#phase 4
try:
    os.mkdir('phase_4')
except:
    pass

path4_plots = Path('phase_4/')

try:
    os.mkdir(os.path.join(path4_plots, 'Data'))
except:
    pass

path4 = Path('phase_4/Data/')



#phase 5
try:
    os.mkdir('phase_5')
except:
    pass

path5_plots = Path('phase_5/')

try:
    os.mkdir(os.path.join(path5_plots, 'Data'))
except:
    pass

path5 = Path('phase_5/Data/')



#phase 6
try:
    os.mkdir('phase_6')
except:
    pass

path6_plots = Path('phase_6/')

try:
    os.mkdir(os.path.join(path6_plots, 'Data'))
except:
    pass

path6 = Path('phase_6/Data/')



#phase 7
try:
    os.mkdir('phase_7')
except:
    pass

path7_plots = Path('phase_7/')

try:
    os.mkdir(os.path.join(path7_plots, 'Data'))
except:
    pass

path7 = Path('phase_7/Data/')



'''
Phase 1
'''
print('Beginning Phase 1')
#Only the primary evolves, and it loses 0.2 Msun
start1 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.8, -0.2/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.2/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.8*np.ones(size1) + q_range1
new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = 0.2
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = m_a/M_b

phase4 = 0 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase1 == 1:
    '''
    Instant Helium Flash
    '''
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
     omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem) \
    = bem.Primary_Instant(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                          dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                          dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1_prem = bem.Primary_Instant_Binding(ams_list1_prem, 0.2, 
                                                    R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                                                    m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                                    q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
     omegapredpointsM_array2_prem, omegapredpointsm_array2_prem) \
    = bem.Primary_Slow(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, 
                       dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2_prem, ams_points_binding2_prem \
    = bem.Primary_Slow_Binding(ams_list2_prem, 
                               R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                               m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                               q3_a, q1_b, q2_b, q3_b, qb, end_time, h)

    '''
    Intermediate Helium Flash
    '''
    (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem) \
    = bem.Primary_Intermediate(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                               m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                               q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3_prem, ams_points_binding3_prem \
    = bem.Primary_Intermediate_Binding(ams_list3_prem, 
                                       R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                                       m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                       q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, ams_list2_prem, arl_list2_prem, 
     arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
     ams_binding2_prem, ams_points_binding2_prem, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
     arl_predpoints_array3_prem, omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, 
     ams_binding3_prem, ams_points_binding3_prem) = bem.Load1(path1)

#%%
'''
Saving Data
'''
bem.Save1(path1, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, 
          ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
          omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_binding2_prem, ams_points_binding2_prem, 
          ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
          omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_binding3_prem, ams_points_binding3_prem)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

test_q = 97

roche_lobe_test1 = bem.roche_lobe(q2_a*q_range1[test_q], R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1_prem[7], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1_prem[17], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2_prem[7], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2_prem[17], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.title('Does Roche Lobe stay under separation, Slow He Flash, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("Time (Years)", fontsize=18)
plt.ylabel("Separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path1_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3_prem[7], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3_prem[17], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3
plt.figure('Red Giant Binary Evolution q dependence far', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
#plt.title('Minimum Separation and Roche Lobe Separation as a function of q', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution q dependence far.png'), bbox_inches='tight')
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution q dependence far.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
#plt.title('Minimum Separation and Roche Lobe Separation as a function of q', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution q dependence close.png'), bbox_inches='tight')
plt.savefig(os.path.join(path1_plots,'Red Giant Binary Evolution q dependence close.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end1 = time.time()

print('the time to run phase 1 was', end1 - start1, 'seconds')



'''
Phase 2
'''
print('Beginning Phase 2')
#Only the primary evolves, but it loses 0.5 Msun instead of 0.2 Msun
start2 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.5, -0.5/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.45, -0.5/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.5/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.5/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 50%

#primary_star
M_b = 0.5*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.5, -0.5/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.45, -0.5/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.5/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.5/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.5*np.ones(size1) + q_range1
new_mass_b = 0.5*(np.ones(size1) + q_range1)
mass_lost_a = 0.5
mass_lost_b = 0.5

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = m_a/M_b

phase4 = 0 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase2 == 1:
    '''
    Instant Helium Flash
    '''
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
     omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem) \
    = bem.Primary_Instant(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                          m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                          q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1_prem = bem.Primary_Instant_Binding(ams_list1_prem, mass_lost_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                                                    dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, 
                                                    dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                                                    q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
     omegapredpointsM_array2_prem, omegapredpointsm_array2_prem) \
    = bem.Primary_Slow(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                       dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2_prem, ams_points_binding2_prem \
    = bem.Primary_Slow_Binding(ams_list2_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, 
                               M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, 
                               q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)

    '''
    Intermediate Helium Flash
    '''
    (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem) \
    = bem.Primary_Intermediate(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                               dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3_prem, ams_points_binding3_prem \
    = bem.Primary_Intermediate_Binding(ams_list3_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, 
                                       dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, 
                                       q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, ams_list2_prem, arl_list2_prem, 
     arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
     ams_binding2_prem, ams_points_binding2_prem, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
     arl_predpoints_array3_prem, omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, 
     ams_binding3_prem, ams_points_binding3_prem) = bem.Load1(path2)

#%%
'''
Saving Data
'''
bem.Save1(path2, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, 
          ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
          omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_binding2_prem, ams_points_binding2_prem, 
          ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
          omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_binding3_prem, ams_points_binding3_prem)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

test_q = 97

roche_lobe_test1 = bem.roche_lobe(q2_a*q_range1[test_q], R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1_prem[7], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1_prem[17], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2_prem[7], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2_prem[17], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.title('Does Roche Lobe stay under separation, M3, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path2_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3_prem[7], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3_prem[17], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3
plt.figure('Red Giant Binary Evolution q dependence far', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
#plt.title('Minimum Separation and Roche Lobe Separation as a function of q', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution q dependence far.png'), bbox_inches='tight')
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution q dependence far.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
#plt.title('Minimum Separation and Roche Lobe Separation as a function of q', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution q dependence close.png'), bbox_inches='tight')
plt.savefig(os.path.join(path2_plots,'Red Giant Binary Evolution q dependence close.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end2 = time.time()

print('the time to run phase 2 was', end2 - start2, 'seconds')



'''
Phase 3
'''
print('Beginning Phase 3')
#Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
#Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
start3 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.8, -0.2/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.2/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.8*np.ones(size1) + q_range1
new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = 0.2
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = m_a/M_b

phase4 = 0 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase3 == 1:
    '''
    Instant Helium Flash
    '''
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
     omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem) \
    = bem.Primary_Instant(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                          m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                          q2_b, q3_b, qb, end_time, h)
    
    (ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, 
     omegapredpointsm_array1, e_list1) \
    = bem.Secondary_Instant(phase4, new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                            arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
                            omegapredpointsm_array1_prem, e_list1_prem, 
                            R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                            dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1_prem = bem.Primary_Instant_Binding(ams_list1_prem, mass_lost_a, R, M_a, M1_a, M2_a, M3_a, 
                                                    m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                                                    dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                                                    q2_b, q3_b, qb, end_time, h)
    
    ams_binding1 = bem.Secondary_Instant_Binding(ams_list1_prem, ams_binding1_prem, mass_lost_b, R, M_a, M1_a, 
                                                 M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                                                 m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                                 q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
     omegapredpointsM_array2_prem, omegapredpointsm_array2_prem) \
    = bem.Primary_Slow(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                       dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2 \
    = bem.Secondary_Slow(phase4, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
                         omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
                         R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                         dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2_prem, ams_points_binding2_prem \
    = bem.Primary_Slow_Binding(ams_list2_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                               m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2, ams_points_binding2 \
    = bem.Secondary_Slow_Binding(ams_binding2_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, 
                                 M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                 q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Intermediate Helium Flash
    '''
    (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem) \
    = bem.Primary_Intermediate(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                               dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3 \
    = bem.Secondary_Intermediate(phase4, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
                                 arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
                                 omegapredpointsm_array3_prem, 
                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                                 q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3_prem, ams_points_binding3_prem \
    = bem.Primary_Intermediate_Binding(ams_list3_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, 
                                       dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                       q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3, ams_points_binding3 \
    = bem.Secondary_Intermediate_Binding(ams_binding3_prem, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, 
                                         dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path3)

#%%
'''
Saving Data
'''
bem.Save2(path3, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
          ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
          arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
          arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
          ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
          arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
          omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
          omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, ams_points_binding3_prem, 
          ams_binding3, ams_points_binding3)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

roche_lobe2 = bem.roche_lobe(1/q_range1, q_range1*R) #initial roche lobe as a function of q for the secondary m

test_q = 97

roche_lobe_test1 = bem.roche_lobe(q2_a*q_range1[test_q], R)

roche_lobe_test2 = bem.roche_lobe(1/(q2_b*q_range1[test_q]), q_range1[test_q]*R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[7], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[17], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[7], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[17], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2[test_q], '--', color='g', label='Secondary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Does Roche Lobe stay under separation, M3, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path3_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[7], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[17], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence far, pre m', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence far pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence far pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence close pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence close pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence far, post m', figsize=(12,12))
plt.plot(q_range1, ams_binding1, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3, '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, ams_binding2, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, pre-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence far post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence far post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of final separation vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence close, post m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3[:,-1], '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, arl_predpoints_array2[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, post-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence close post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path3_plots,'Red Giant Binary Evolution q dependence close post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end3 = time.time()

print('the time to run phase 3 was', end3 - start3, 'seconds')



'''
Phase 4
'''
print('Beginning Phase 4')
#Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
#Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
#As well as this, the separation is fit to prevent the secondary from causing mass transfer to happen when it evolves.
start4 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.8, -0.2/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.2/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.8*np.ones(size1) + q_range1
new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = 0.2
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = m_a/M_b

phase4 = 1 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase4 == 1:
    '''
    Instant Helium Flash
    '''
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
     omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem) \
    = bem.Primary_Instant(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                          m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                          q2_b, q3_b, qb, end_time, h)
    
    (ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1) \
    = bem.Secondary_Instant(phase4, new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                            arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
                            omegapredpointsm_array1_prem, e_list1_prem, 
                            R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                            m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                            q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1_prem = bem.Primary_Instant_Binding(ams_list1_prem, mass_lost_a, 
                                                    R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, 
                                                    M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, 
                                                    size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1 = bem.Secondary_Instant_Binding(ams_list1_prem, ams_binding1_prem, mass_lost_b, 
                                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                                                 m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                                 q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
     omegapredpointsM_array2_prem, omegapredpointsm_array2_prem) \
    = bem.Primary_Slow(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, 
                       dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2 \
    = bem.Secondary_Slow(phase4, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
                         omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
                         R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                         dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2_prem, ams_points_binding2_prem \
    = bem.Primary_Slow_Binding(ams_list2_prem, 
                               R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                               dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2, ams_points_binding2 \
    = bem.Secondary_Slow_Binding(ams_binding2_prem, 
                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                 q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Intermediate Helium Flash
    '''
    (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem) \
    = bem.Primary_Intermediate(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                               m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, 
                               q3_b, qb, end_time, h)
    
    ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3 \
    = bem.Secondary_Intermediate(phase4, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
                                 arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
                                 omegapredpointsm_array3_prem, 
                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                                 m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, 
                                 q3_b, qb, end_time, h)
    
    ams_binding3_prem, ams_points_binding3_prem \
    = bem.Primary_Intermediate_Binding(ams_list3_prem, 
                                       R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                                       m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, 
                                       q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3, ams_points_binding3 \
    = bem.Secondary_Intermediate_Binding(ams_binding3_prem, 
                                         R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                                         m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path4)

#%%
'''
Saving Data
'''
bem.Save2(path4, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
          ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
          arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
          arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
          ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
          arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
          omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
          omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, ams_points_binding3_prem, 
          ams_binding3, ams_points_binding3)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

roche_lobe2 = bem.roche_lobe(1/q_range1, q_range1*R) #initial roche lobe as a function of q for the secondary m

test_q = 97

roche_lobe_test1 = bem.roche_lobe(q2_a*q_range1[test_q], R)

roche_lobe_test2 = bem.roche_lobe(1/(q2_b*q_range1[test_q]), q_range1[test_q]*R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[7], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[17], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[7], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[17], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2[test_q], '--', color='g', label='Secondary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Does Roche Lobe stay under separation, M3, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[7], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[7], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[17], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[17], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence far, pre m', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence far pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence far pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence far, post m', figsize=(12,12))
plt.plot(q_range1, ams_binding1, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3, '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, ams_binding2, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, pre-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence far post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence far post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of final separation vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence close, post m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3[:,-1], '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, arl_predpoints_array2[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, post-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

'''
making the last close plot but with period instead of separation
'''
period1 = np.copy(arl_predpoints_array1[:,-1])
period2 = np.copy(arl_predpoints_array2[:,-1])
period3 = np.copy(arl_predpoints_array3[:,-1])

sep1 = np.copy(arl_predpoints_array1[:,-1])
sep2 = np.copy(arl_predpoints_array2[:,-1])
sep3 = np.copy(arl_predpoints_array3[:,-1])

bem.Period_Convert(period1, period2, period3, sep1, sep2, sep3, q_range1, size1)

#making a plot of final separation vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence close, post m, period', figsize=(12,12))
plt.plot(q_range1, period1, '-', color='b', label='Minimum Orbital Period, Instant He Flash')
plt.plot(q_range1, period3, '--', color='g', label='Minimum Orbital Period, Intermediate He Flash')
plt.plot(q_range1, period2, ':', color='r', label='Minimum Orbital Period, Slow He Flash')
#plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
#plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Orbital Period as a function of q, post-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("Orbital Period (Years)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close post m p.png'), bbox_inches='tight')
plt.savefig(os.path.join(path4_plots,'Red Giant Binary Evolution q dependence close post m p.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end4 = time.time()

print('the time to run phase 4 was', end4 - start4, 'seconds')



'''
Phase 5
'''
print('Beginning Phase 5')
#Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
#Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
#For q>0.97, primary and secondary evolution is replaced with the primary and secondary evolving at the same time.
start5 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.8, -0.2/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.2/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.8*np.ones(size1) + q_range1
new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = 0.2*(np.ones(size1) + q_range1)
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = np.ones(N+1) #will always be 1 scaled by the starting mass ratio since the primary and secondary lose mass at a proportionate rate

phase4 = 0 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase5 == 1:
    #Since phase 5 is an overwrite of phase 3, start by loading phase 3
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path3)
    
    '''
    Instant Helium Flash
    '''
    bem.Overwrite_Instant(new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                          arl_predpoints_array1_prem, omegapredpointsM_array1_prem, ams_list1, arl_list1, 
                          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, 
                          e_list1, omegapredpointsm_array1_prem, e_list1_prem, R, M_a, M1_a, M2_a, M3_a, m_a, 
                          dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, 
                          size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Instant_Binding(ams_list1_prem, ams_list1, ams_binding1_prem, ams_binding1, mass_lost_a, 
                                  R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                                  m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                  q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    bem.Overwrite_Slow(ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
                       omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, arl_list2, 
                       arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
                       R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                       dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Slow_Binding(ams_list2_prem, ams_list2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
                         ams_points_binding2, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                         m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                         q2_b, q3_b, qb, end_time, h)
    
    '''
    Intermediate Helium Flash
    '''
    bem.Overwrite_Intermediate(ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
                               omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, 
                               arl_pred_list3, arl_predpoints_array3, omegapredpointsM_array3, 
                               omegapredpointsm_array3, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, 
                               M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Intermediate_Binding(ams_list3_prem, ams_list3, ams_binding3_prem, ams_points_binding3_prem, 
                                   ams_binding3, ams_points_binding3, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                                   dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, 
                                   q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path5)

#%%
'''
Saving Data
'''
bem.Save2(path5, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
          ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
          arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
          arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
          ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
          arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
          omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
          omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, ams_points_binding3_prem, 
          ams_binding3, ams_points_binding3)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

roche_lobe2 = bem.roche_lobe(1/q_range1, q_range1*R) #initial roche lobe as a function of q for the secondary m

test_q = 98

roche_lobe_test1 = bem.roche_lobe(qb*q_range1[test_q], R)

roche_lobe_test2 = bem.roche_lobe(1/(qb*q_range1[test_q]), q_range1[test_q]*R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[97], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[98], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[97], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[98], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2[test_q], '--', color='g', label='Secondary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Does Roche Lobe stay under separation, M2, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path5_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[97], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[98], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence far, pre m', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence far pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence far pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence close pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence close pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence far, post m', figsize=(12,12))
plt.plot(q_range1, ams_binding1, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3, '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, ams_binding2, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, pre-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence far post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence far post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of final separation vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence close, post m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3[:,-1], '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, arl_predpoints_array2[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, post-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence close post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path5_plots,'Red Giant Binary Evolution q dependence close post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end5 = time.time()

print('the time to run phase 5 was', end5 - start5, 'seconds')



'''
Phase 6
'''
print('Beginning Phase 6')
#Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
#Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
#As well as this, the separation is fit to prevent the secondary from causing mass transfer to happen when it evolves.
#For q>0.97, primary and secondary evolution is replaced with the primary and secondary evolving at the same time.
start6 = time.time()

#initial properties of system
R = 100 #Rsun, this is 1 AU
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones(N+1)
M2_a = np.arange(1, 0.8, -0.2/(N+1)) 
M3_a = M_a*np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])#this step size makes it so that the final array has N+1 elements

#secondary star
m_a = np.ones(N+1)

#mass derivatives
dM1_a = np.zeros(N+1)/conversion
dM2_a = (-0.2/(N+1))*np.ones(N+1)/conversion
dM3_a = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion
dm_a = np.zeros(N+1)/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.arange(0.01, 1.00, 0.01)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = 0.8*np.ones(size1) + q_range1
new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = 0.2*(np.ones(size1) + q_range1)
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = np.ones(N+1) #will always be 1 scaled by the starting mass ratio since the primary and secondary lose mass at a proportionate rate

phase4 = 1 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase6 == 1:
    #Since phase 5 is an overwrite of phase 3, start by loading phase 3
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path4)
    
    '''
    Instant Helium Flash
    '''
    bem.Overwrite_Instant(new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                          arl_predpoints_array1_prem, omegapredpointsM_array1_prem, ams_list1, arl_list1, 
                          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, 
                          e_list1, omegapredpointsm_array1_prem, e_list1_prem, R, M_a, M1_a, M2_a, M3_a, m_a, 
                          dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, 
                          size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Instant_Binding(ams_list1_prem, ams_list1, ams_binding1_prem, ams_binding1, mass_lost_a, 
                                  R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                                  m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                  q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    bem.Overwrite_Slow(ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
                       omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, arl_list2, 
                       arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
                       R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                       dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Slow_Binding(ams_list2_prem, ams_list2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
                         ams_points_binding2, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                         m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                         q2_b, q3_b, qb, end_time, h)
    
    '''
    Intermediate Helium Flash
    '''
    bem.Overwrite_Intermediate(ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
                               omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, 
                               arl_pred_list3, arl_predpoints_array3, omegapredpointsM_array3, 
                               omegapredpointsm_array3, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, 
                               M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, 
                               q1_b, q2_b, q3_b, qb, end_time, h)
    
    bem.Overwrite_Intermediate_Binding(ams_list3_prem, ams_list3, ams_binding3_prem, ams_points_binding3_prem, 
                                   ams_binding3, ams_points_binding3, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                                   dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, 
                                   q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
     omegapredpointsM_array1, omegapredpointsm_array1, e_list1, ams_binding1_prem, ams_binding1, ams_list2_prem, 
     arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, 
     omegapredpointsm_array2_prem, ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
     omegapredpointsM_array2, omegapredpointsm_array2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
     ams_points_binding2, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, 
     arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
     ams_points_binding3_prem, ams_binding3, ams_points_binding3) = bem.Load2(path6)

#%%
'''
Saving Data
'''
bem.Save2(path6, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
          ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
          arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
          arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
          ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
          arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
          omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
          omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, ams_points_binding3_prem, 
          ams_binding3, ams_points_binding3)

#%%

'''
Plots
'''
roche_lobe1 = bem.roche_lobe(q_range1, R) #initial roche lobe as a function of q for the primary M

roche_lobe2 = bem.roche_lobe(1/q_range1, q_range1*R) #initial roche lobe as a function of q for the secondary m

test_q = 98

roche_lobe_test1 = bem.roche_lobe(qb*q_range1[test_q], R)

roche_lobe_test2 = bem.roche_lobe(1/(qb*q_range1[test_q]), q_range1[test_q]*R)



#plots for M1
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[97], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots, 'Red Giant Binary Evolution 1, M1.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M1', figsize=(12,12))
plt.plot(np.arange(0, end_time + 2*h, h), arl_predpoints_array1[98], '-', color='b')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 2, M1.png'), bbox_inches='tight')
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 2, M1.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M2
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[97], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 1, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array2[98], '--', color='g')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 2, M2.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Does Roche Lobe stay under separation, M2', figsize=(12,12))
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2_prem[test_q], '-', color='g', label='Primary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), arl_predpoints_array2[test_q], '--', color='g', label='Secondary Evolution')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(np.arange(0, end_time + 1*h, h), roche_lobe_test2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Does Roche Lobe stay under separation, M2, q=' +str(round(q_range1[test_q], 3)), fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path6_plots,'Does Roche Lobe stay under separation, M2.png'), bbox_inches='tight')
plt.show()
plt.close()



#plots for M3
#%%
#making plots of separation and angular velocity vs time
plt.figure('Red Giant Binary Evolution 1, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[97], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[97], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 1, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

#%%
plt.figure('Red Giant Binary Evolution 2, M3', figsize=(12,12))
plt.plot(np.arange(0, end_time + h, h), arl_predpoints_array3[98], ':', color='r')
plt.title('Red Giant Binary Evolution, q=' +str(round(q_range1[98], 3)), fontsize=18)
plt.xlabel("time (years)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution 2, M3.png'), bbox_inches='tight')
plt.show()
plt.close()

'''
#plots for all 3
'''
#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence far, pre m', figsize=(12,12))
plt.plot(q_range1, ams_binding1_prem, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3_prem, '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, ams_binding2_prem, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1_prem, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3_prem, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2_prem, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence far pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence far pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()



#%%
#making a plot of final separation vs q for M1 and M2 and M3, pre m evolution
plt.figure('Red Giant Binary Evolution q dependence close, pre m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1_prem[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3_prem[:,-1], '--', color='g', label='Minimum Separation, Middle He Flash')
plt.plot(q_range1, arl_predpoints_array2_prem[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
plt.title('Minimum Separation and Roche Lobe Separation as a function of q, pre m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.legend()
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence close pre m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence close pre m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of initial and roche lobe vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence far, post m', figsize=(12,12))
plt.plot(q_range1, ams_binding1, '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, ams_binding3, '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, ams_binding2, ':', color='r', label='Minimum Separation, Slow He Flash')
#plt.plot(q_range1, ams_list1, label='Minimum Separation old, Instant He Flash')
#plt.plot(q_range1, ams_list3, label='Minimum Separation old, Middle He Flash')
#plt.plot(q_range1, ams_list2, label='Minimum Separation old, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, pre-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence far post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence far post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of final separation vs q for M1 and M2 and M3, post m evolution
plt.figure('Red Giant Binary Evolution q dependence close, post m', figsize=(12,12))
plt.plot(q_range1, arl_predpoints_array1[:,-1], '-', color='b', label='Minimum Separation, Instant He Flash')
plt.plot(q_range1, arl_predpoints_array3[:,-1], '--', color='g', label='Minimum Separation, Intermediate He Flash')
plt.plot(q_range1, arl_predpoints_array2[:,-1], ':', color='r', label='Minimum Separation, Slow He Flash')
plt.plot(q_range1, roche_lobe1, '-.', color='k', label='Roche Lobe Separation for Primary')
plt.plot(q_range1, roche_lobe2, '.', color='k', label='Roche Lobe Separation for Secondary')
#plt.title('Minimum Separation as a function of q, post-tides, post m', fontsize=18)
plt.xlabel("q (mass ratio)", fontsize=18)
plt.ylabel("separation (Rsun)", fontsize=18)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend()
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence close post m.png'), bbox_inches='tight')
plt.savefig(os.path.join(path6_plots,'Red Giant Binary Evolution q dependence close post m.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end6 = time.time()

print('the time to run phase 6 was', end6 - start6, 'seconds')



'''
Phase 7
'''
print('Beginning Phase 7')
#Only the primary evolves, and it loses 0.2 Msun
start7 = time.time()

#initial properties of system
R = np.linspace(40, 220, 10) #Rsun
end_time = 100000.0 #time when evolution ends in years; it starts at 0 years.
N = 200 #number of steps for RK4
h = end_time/N #size of each step for RK4
conversion = end_time/(N+1) #units of time/number, will be used so that dM/dt has units mass/time

#primary star
new_mass_array = np.linspace(0.95, 0.5, 10) #final mass, Msun
size4 = len(new_mass_array)
M_a = 1 #Msun, mass of the primary
M1_a = M_a*np.ones((size4, N+1))
M2_a = np.ones((size4, N+1))
for k in range(size4):
    M2_a[k] = np.linspace(1.0, new_mass_array[k], N+1, endpoint=True)

M3_a = np.ones((size4, N+1)) 
for k in range(size4): #this step size makes it so that the final array has N+1 elements
    M3_a[k] = M_a*np.hstack([np.ones(N-3), np.linspace(1.0, new_mass_array[k], 4, endpoint=True)])

#secondary star
m_a = np.ones((size4, N+1))

#mass derivatives
dM1_a = np.zeros((size4, N+1))/conversion
dM2_a = np.ones((size4, N+1))
for k in range(size4):
    dM2_a[k] = ((new_mass_array[k] - 1)/(N+1))*np.ones(N+1)/conversion

dM3_a = np.ones((size4, N+1))
for k in range(size4):
    dM3_a[k] = np.hstack([np.zeros(N-3), ((new_mass_array[k] - 1)/4)*np.ones(4)/conversion])

dm_a = np.zeros((size4, N+1))/conversion



#next, build the mass functions for when m is evolving. Need multiple instances for m, since it only 
#loses mass above q=0.8 
#Also note that the secondary star loses mass the same way as its primary did, but scaled by q
#so relative mass loss is the same: 20%

#primary_star
M_b = 0.8*np.ones(N+1)

#secondary star, q>0.8
m1_b = np.ones(N+1)
m2_b = np.arange(1, 0.8, -0.2/(N+1))
m3_b = np.hstack([np.ones(int(N-3)), np.arange(0.95, 0.75, -0.2/4)])

#for q<0.8, use m_a, as it will be the same for both before and after.

#mass derivatives, for q>0.8 (use dm_a for q<0.8)
dM_b = np.zeros(N+1)/conversion
dm1_b = np.zeros(N+1)/conversion
dm2_b = (-0.2/(N+1))*np.ones(N+1)/conversion
dm3_b = np.hstack([np.zeros(int(N-3)), (-0.2/4)*np.ones(4)])/conversion



#now building the q arrays for each case

q_range1 = np.linspace(0.2, 1.0, 5, endpoint=True)
size1 = len(q_range1)

#instant helium flash mass
new_mass_a = np.ones((size1, size4))
for k in range(size4):
    new_mass_a[:,k] = new_mass_array[k]*np.ones(size1) + q_range1

new_mass_b = 0.8*(np.ones(size1) + q_range1)
mass_lost_a = np.linspace(1.0 - new_mass_array[0], 1.0 - new_mass_array[-1], len(new_mass_array), endpoint=True)
mass_lost_b = 0.2*q_range1

q1_a = m_a/M1_a
q2_a = m_a/M2_a
q3_a = m_a/M3_a

q1_b = m1_b/M_b
q2_b = m2_b/M_b
q3_b = m3_b/M_b

qb = m_a/M_b

phase4 = 0 #whether or not to do the phase 4 adjustments; 1 for phase 4, 0 elsewise

if do_phase7 == 1:
    '''
    Instant Helium Flash
    '''
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
     omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem) \
    = bem.Primary_Instant_Custom(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                          dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, 
                          dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding1_prem = bem.Primary_Instant_Binding_Custom(ams_list1_prem, mass_lost_a, 
                                                    R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                                                    m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                                    q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    '''
    Slow Helium Flash
    '''
    (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
     omegapredpointsM_array2_prem, omegapredpointsm_array2_prem) \
    = bem.Primary_Slow_Custom(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                              m2_b, m3_b, dM_b, dm1_b, 
                       dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding2_prem, ams_points_binding2_prem \
    = bem.Primary_Slow_Binding_Custom(ams_list2_prem, 
                               R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, 
                               m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                               q3_a, q1_b, q2_b, q3_b, qb, end_time, h)

    '''
    Intermediate Helium Flash
    '''
    (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
     omegapredpointsM_array3_prem, omegapredpointsm_array3_prem) \
    = bem.Primary_Intermediate_Custom(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                               m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                               q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
    
    ams_binding3_prem, ams_points_binding3_prem \
    = bem.Primary_Intermediate_Binding_Custom(ams_list3_prem, 
                                       R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                                       m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, 
                                       q3_a, q1_b, q2_b, q3_b, qb, end_time, h)
else:
    (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
     omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, ams_list2_prem, arl_list2_prem, 
     arl_pred_list2_prem, arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
     ams_binding2_prem, ams_points_binding2_prem, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
     arl_predpoints_array3_prem, omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, 
     ams_binding3_prem, ams_points_binding3_prem) = bem.Load1(path7)

#%%
'''
Saving Data
'''
bem.Save1(path7, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, 
          ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
          omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_binding2_prem, ams_points_binding2_prem, 
          ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
          omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_binding3_prem, ams_points_binding3_prem)

#%%
'''
Plots
'''

#%%
#making a plot of Radius and Mass loss, Instant Helium Flash, q=0.2
plt.figure('Red Giant Binary Evolution Instant Small q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding1_prem[0]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation Instant Helium Flash, q=0.2, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array1_prem[0,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation Instant Helium Flash, q=0.2, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Small q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Small q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Intermediate Helium Flash, q=0.2
plt.figure('Red Giant Binary Evolution Intermediate Small q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding3_prem[0]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array3_prem[0,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Small q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Small q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Slow Helium Flash, q=0.2
plt.figure('Red Giant Binary Evolution Slow Small q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding2_prem[0]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array2_prem[0,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Small q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Small q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Instant Helium Flash, q=0.6
plt.figure('Red Giant Binary Evolution Instant Medium q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding1_prem[2]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array1_prem[2,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Medium q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Medium q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Intermediate Helium Flash, q=0.6
plt.figure('Red Giant Binary Evolution Intermediate Small q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding3_prem[2]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array3_prem[2,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Medium q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Medium q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Slow Helium Flash, q=0.6
plt.figure('Red Giant Binary Evolution Slow Medium q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding2_prem[2]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array2_prem[2,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Medium q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Medium q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Instant Helium Flash, q=1.0
plt.figure('Red Giant Binary Evolution Instant Large q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding1_prem[4]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array1_prem[4,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Large q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Instant Large q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Intermediate Helium Flash, q=1.0
plt.figure('Red Giant Binary Evolution Intermediate Large q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding3_prem[4]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array3_prem[4,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Large q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Intermediate Large q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%
#making a plot of Radius and Mass loss, Slow Helium Flash, q=1.0
plt.figure('Red Giant Binary Evolution Slow Large q', figsize=(20,8))
plt.subplot(121)
#close plot
plt.imshow(np.log10(ams_binding2_prem[4]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, pre-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

#far plot
plt.subplot(122)
plt.imshow(np.log10(arl_predpoints_array2_prem[4,:,:,-1]), origin='lower', aspect='auto')
plt.clim(1, 4)
plt.colorbar().set_label('Ln Separation (Rsun)', fontsize=18)
plt.title('Minimum Separation as a function of q, post-tides', fontsize=18)
plt.xlabel("Mass Loss (Msun)", fontsize=18)
plt.ylabel("Radius (Rsun)", fontsize=18)
plt.xticks(np.arange(0, len(mass_lost_a), 1), np.round(mass_lost_a, 2), fontsize=14)
plt.yticks(np.arange(0, len(R), 1), R, fontsize=14)

plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Large q.png'), bbox_inches='tight')
plt.savefig(os.path.join(path7_plots,'Red Giant Binary Evolution Slow Large q.pdf'), bbox_inches='tight')
plt.show()
plt.close()


#%%

end7 = time.time()

print('the time to run phase 7 was', end7 - start7, 'seconds')

end = time.time()

print('the time to run this was', end - start, 'seconds')

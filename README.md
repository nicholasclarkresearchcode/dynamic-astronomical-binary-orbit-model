The code is split into two chunks. BEM_analysis_functions defines each function used in the code, while BEM_analysis runs the 
functions and makes plots.

The code output is split into 7 "phases", of varying complexity. Currently only phase 1 and 7 are in use, and the others 
serve for bugtesting purposes mainly, to make sure the outputs are reasonable for more complicated situations.

A description of what each phase does is found in BEM_analysis, in the beginning of the section where the code for that phase is.

Here they are:
phase 1: Only the primary evolves, and it loses 0.2 Msun

phase 2: Only the primary evolves, but it loses 0.5 Msun instead of 0.2 Msun

phase 3: Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.

phase 4: Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
As well as this, the separation is fit to prevent the secondary from causing mass transfer to happen when it evolves.

phase 5: Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
for q>0.97, primary and secondary evolution is replaced with the primary and secondary evolving at the same time.

phase 6: Primary evolves and loses 0.2 Msun, and then secondary evolves from where primary left off.
Secondary has a mass radius relation where radius is scaled by the mass ratio, and it loses 20% of its mass.
As well as this, the separation is fit to prevent the secondary from causing mass transfer to happen when it evolves.
For q>0.97, primary and secondary evolution is replaced with the primary and secondary evolving at the same time.

phase 7: #Only the primary evolves, and it loses a variable amount of mass
Now exploring a more complete parameter space, with the initial radius of the primary
varying



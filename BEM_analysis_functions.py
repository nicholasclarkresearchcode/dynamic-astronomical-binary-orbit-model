# -*- coding: utf-8 -*-
"""
Created on Mon May  3 15:59:06 2021

@author: nickj
"""

import numpy as np
import os



'''
Initial Constants
'''
#Working with units of years, Msun, and Rsun
Rsun = 6.96e8 #metres, radius of the sun
Msun = 1.989e30 #kg, mass of the sun
yr = 3.1536e7 #seconds in a year
k = 0.2 #const ranging from 0.1 to 0.01; Prof Neilson uses 0.2 in his 2015 paper so this is done here also.
G = 6.67408e-11*Msun*yr**2/(Rsun**3) #gravitational constant



'''
Sub functions that are used in the systems of ODEs
'''
def n(a, M, q): #orbital angular velocity of the binary system
    m = M*q #mass of secondary star
    y = ((G*(M + m))**0.5)/a**1.5
    return y

def f1(e): #eccentricity polynomials. Note that these are all 1 when e=0
    y = 1 + (31*e**2)/2 + (255*e**4)/8 + (185*e**6)/16 + (25*e**8)/64
    return y

def f2(e):
    y = 1 + (15*e**2)/2 + (45*e**4)/8 + (5*e**6)/16
    return y

def f3(e):
    y = 1 + (15*e**2)/4 + (15*e**4)/8 + (5*e**6)/64
    return y

def f4(e):
    y = 1 + (3*e**2)/2 + (e**4)/8
    return y

def f5(e):
    y = 1 +3*e**2 + (3*e**4)/8
    return y
      

 
'''
System of ODE functions
'''
#r[0] = a the separation, r[1] = omegaM the rotational angular velocity of the primary, 
#r[2] = e the eccentricity, r[3] = omegam the rotational angular velocity of the secondary

#System of ODEs for when the primary and secondary are evolving at the same time
def f(r, M, q, R, Rm, TM, Tm, dm, dM): 
    a = r[0]
    omegaM = r[1]
    e = r[2]
    omegam = r[3]
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
  
    #tidal equations, with da added to fa
    #separation. 1st term is for primary, 2nd term is for seconadry, 3rd term is binding energy
    fa = (-6*((k*q*(1 + q)*(R**8))/TM) * (f1(e) - f2(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (1/(a**7)) * (1/((1-e**2)**7.5))
    -6*((k*qm*(1 + qm)*(Rm**8))/Tm) * (f1(e) - f2(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (1/(a**7)) * (1/((1-e**2)**7.5)) 
    - ((1+e)/(1-e))*((a)/(m+M))*(dm+dM)) 
    
    #the rotational angular velocity for the primary
    fomegaM = 3*(((q**2)*(R**6))/TM) * (f2(e) - f5(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (n(a, M, q)/a**6) * (1/((1-e**2)**6)) 
    
    #eccentricity. 1st term is for primary, 2nd term is for secodary
    fe = (-27*((k*q*(1 + q)*(R**8))/TM) * (f3(e) - (11/18)*f4(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (e/a**8) * (1/((1-e**2)**6.5))
    -27*((k*qm*(1 + qm)*(R**8))/Tm) * (f3(e) - (11/18)*f4(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (e/a**8) * (1/((1-e**2)**6.5))) 
    
    #the rotational angular velocity for the secondary
    fomegam = 3*(((qm**2)*(Rm**6))/Tm) * (f2(e) - f5(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (n(a, M, q)/a**6) * (1/((1-e**2)**6)) 
    
    return np.array([fa, fomegaM, fe, fomegam])



#System of ODEs for when the primary is evolving, and the secondary is a point mass for the tides
def fM(r, M, q, R, TM, dm, dM):
    a = r[0]
    omegaM = r[1]
    e = r[2]
    m = q*M #mass of the secondary
  
    #tidal equations, with da added to fa
    #separation. 1st term is for primary, 2nd term is binding energy
    fa = (-6*((k*q*(1 + q)*(R**8))/TM) * (f1(e) - f2(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (1/(a**7)) * (1/((1-e**2)**7.5))
    - ((1+e)/(1-e))*((a)/(m+M))*(dm+dM))
    
    #the rotational angular velocity for the primary
    fomegaM = 3*(((q**2)*(R**6))/TM) * (f2(e) - f5(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (n(a, M, q)/a**6) * (1/((1-e**2)**6))
    
    #eccentricity for the primary
    fe = -27*((k*q*(1 + q)*(R**8))/TM) * (f3(e) - (11/18)*f4(e)*((1-e**2)**1.5)*omegaM/(n(a, M, q))) * (e/a**8) * (1/((1-e**2)**6.5))
    
    #the rotational angular velocity for the secondary
    fomegam = 0
    
    return np.array([fa, fomegaM, fe, fomegam])



#System of ODEs for when the secondary is evolving, and the primary is a point mass for the tides
def fm(r, M, q, R, Tm, dm, dM):
    a = r[0]
    e = r[2]
    omegam = r[3]
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
  
    #tidal equations, with da added to fa
    #separation. 1st term is for secondary, 2nd term is binding energy
    fa =  (-6*((k*qm*(1 + qm)*(R**8))/Tm) * (f1(e) - f2(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (1/(a**7)) * (1/((1-e**2)**7.5))
    - ((1+e)/(1-e))*((a)/(m+M))*(dm+dM))
    
    #the rotational angular velocity for the primary
    fomegaM = 0 
    
    #eccentricity for the secondary
    fe = -27*((k*qm*(1 + qm)*(R**8))/Tm) * (f3(e) - (11/18)*f4(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (e/a**8) * (1/((1-e**2)**6.5))
    
    #the rotational angular velocity for the secondary
    fomegam = 3*(((qm**2)*(R**6))/Tm) * (f2(e) - f5(e)*((1-e**2)**1.5)*omegam/(n(a, M, q))) * (n(a, M, q)/a**6) * (1/((1-e**2)**6))
    
    return np.array([fa, fomegaM, fe, fomegam])


'''
#Functions that solve systems of ODEs using the 4th order Runge-Kutta method
'''
#Solves the f system of ODEs
def minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h):
    r = np.array([ams, 0.0, 0.0, 0.0]) #initial omega is 0 by assumption, for both M and m
    time = np.arange(0.0, end_time, h) #time array for RK4 method
    
    #making lists and adding initial values to them
    Rpoints = []
    omegaMpoints = []
    omegampoints = []
    Rpoints.append(ams)
    omegaMpoints.append(0.0)
    omegampoints.append(0.0)
    
    #RK4 Method
    for t in range(len(time)):
        t = t+1 #t shifted forward so that the correct values are called for a given r.
        if r[0] > R: #program stops if theres a collision, defined to be the separation less than the radius of the primary.
           k1 = h*f(r, M[t], q[t], R, Rm, TM[t], Tm[t], dm[t], dM[t])
           k2 = h*f(r + 0.5*k1, M[t], q[t], R, Rm, TM[t], Tm[t], dm[t], dM[t])
           k3 = h*f(r + 0.5*k2, M[t], q[t], R, Rm, TM[t], Tm[t], dm[t], dM[t])
           k4 = h*f(r + k3, M[t], q[t], R, Rm, TM[t], Tm[t], dm[t], dM[t])
           r += (k1 + 2*k2 + 2*k3 + k4)/6 
        else:
            r = 0.0, 0.0, 0.0, 0.0
    
        #adding most recent values to lists
        Rpoints.append(r[0])
        omegaMpoints.append(r[1])
        omegampoints.append(r[3])
    #turning lists into arrays
    Rpoints = np.array(Rpoints)
    omegaMpoints = np.array(omegaMpoints)
    omegampoints = np.array(omegampoints)

    #python thinks nan is a min, so remove these
    for i in range(np.size(Rpoints)):
        if np.isnan(Rpoints[i]) == True:
            Rpoints[i] = 0
            
    #return the minimum point of r[0], alongside arrays and final eccentricity
    return np.min(Rpoints), Rpoints, omegaMpoints, r[2], omegampoints



#Solves the fM system of ODEs
def minsepM(ams, M, q, R, TM, dm, dM, end_time, h):
    r = np.array([ams, 0.0, 0.0, 0.0]) #initial omega is 0 by assumption, for both M and m
    time = np.arange(0.0, end_time, h) #time array for RK4 method
        
    #making lists and adding initial values to them
    Rpoints = []
    omegaMpoints = []
    omegampoints = []
    Rpoints.append(ams)
    omegaMpoints.append(0.0)
    omegampoints.append(0.0)

    #RK4 Method
    for t in range(len(time)):
        t = t+1 #t shifted forward so that the correct values are called for a given r.
        if r[0] > R: #program stops if theres a collision, defined to be the separation less than the radius of the primary.
           k1 = h*fM(r, M[t], q[t], R, TM[t], dm[t], dM[t])
           k2 = h*fM(r + 0.5*k1, M[t], q[t], R, TM[t], dm[t], dM[t])
           k3 = h*fM(r + 0.5*k2, M[t], q[t], R, TM[t], dm[t], dM[t])
           k4 = h*fM(r + k3, M[t], q[t], R, TM[t], dm[t], dM[t])
           r += (k1 + 2*k2 + 2*k3 + k4)/6 
        else:
            r = 0.0, 0.0, 0.0, 0.0

        #adding most recent values to lists
        Rpoints.append(r[0])
        omegaMpoints.append(r[1])
        omegampoints.append(r[3])
    #turning lists into arrays
    Rpoints = np.array(Rpoints)
    omegaMpoints = np.array(omegaMpoints)
    omegampoints = np.array(omegampoints)

    #python thinks nan is a min, so remove these
    for i in range(np.size(Rpoints)):
        if np.isnan(Rpoints[i]) == True:
            Rpoints[i] = 0
            
    #return the minimum point of r[0], alongside arrays and final eccentricity            
    return np.min(Rpoints), Rpoints, omegaMpoints, r[2], omegampoints



#Solves the fm system of ODEs
def minsepm(ams, M, q, R, Tm, dm, dM, end_time, h):
    r = np.array([ams, 0.0, 0.0, 0.0]) #initial omega is 0 by assumption, for both M and m
    time = np.arange(0.0, end_time, h) #time array for RK4 method
        
    #making lists and adding initial values to them
    Rpoints = []
    omegaMpoints = []
    omegampoints = []
    Rpoints.append(ams)
    omegaMpoints.append(0.0)
    omegampoints.append(0.0)

    #RK4 Method
    for t in range(len(time)):
        t = t+1 #t shifted forward so that the correct values are called for a given r.
        if r[0] > R: #program stops if theres a collision, defined to be the separation less than the radius of the primary.
           k1 = h*fm(r, M[t], q[t], R, Tm[t], dm[t], dM[t])
           k2 = h*fm(r + 0.5*k1, M[t], q[t], R, Tm[t], dm[t], dM[t])
           k3 = h*fm(r + 0.5*k2, M[t], q[t], R, Tm[t], dm[t], dM[t])
           k4 = h*fm(r + k3, M[t], q[t], R, Tm[t], dm[t], dM[t])
           r += (k1 + 2*k2 + 2*k3 + k4)/6 
        else:
            r = 0.0, 0.0, 0.0, 0.0

        #adding most recent values to lists
        Rpoints.append(r[0])
        omegaMpoints.append(r[1])
        omegampoints.append(r[3])
    #turning lists into arrays
    Rpoints = np.array(Rpoints)
    omegaMpoints = np.array(omegaMpoints)
    omegampoints = np.array(omegampoints)

    #python thinks nan is a min, so remove these
    for i in range(np.size(Rpoints)):
        if np.isnan(Rpoints[i]) == True:
            Rpoints[i] = 0
            
    #return the minimum point of r[0], alongside arrays and final eccentricity            
    return np.min(Rpoints), Rpoints, omegaMpoints, r[2], omegampoints



'''
Functions that take solved systems of ODEs and apply the condition that the separation must be greater than
the Roche Lobe separation, while at the same time getting as close as possible to it
'''
#Function containing some prerequesite arrays used in the various minsep_full functions
def Minsep_details(M, q, R):
    #Roche Lobe radius. Note that the Roche Lobe radius has units of stellar separation = 1
    rl = (0.49*q**(-2/3))/(0.69*q**(-2/3) + np.log(1 + q**(-1/3)))
    
    #Roche Lobe Separation; the separation when mass transfer occurs
    arl = R/rl #units of solar radii
    m = q*M #mass of the secondary
    
    #the orbital period of a particle in a glancing collision around the star;
    #the separation of this is taken to be the Roche Lobe Separation.
    #since Ps depends on M, it will change with time and so an array needs to be made
    Ps = np.zeros(len(M))
    for i in range(len(M)):
        Ps[i] = ((4*(np.pi**2)*(arl[i]**3))/(G*(M[i]+m[i])))**0.5
    Tratio = 0.001 #ratio between Ps and the time lag tau, varies from 0 to 0.5
    T = Ps/((4*np.pi**2)*(Tratio)) #time scale on which significant change happens in the orbit
    
    return arl, T



#Roche Lobe function, for plotting
def roche_lobe(q, R):
    #Roche Lobe radius. Note that the Roche Lobe radius has units of stellar separation = 1
    rl = (0.49*q**(-2/3))/(0.69*q**(-2/3) + np.log(1 + q**(-1/3)))
    
    #Roche Lobe Separation; the separation when mass transfer occurs
    arl = R/rl #units of solar radii
    return arl


'''NOTE: make it so that the functions that have primary and secondary evolving at the same time take 2 radius args'''
#function for when the primary and secondary evolve at the same rate, continuous mass loss
def Minsep_full(M, q, R, dm, dM, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, TM = Minsep_details(M, q, R)
    
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R) #applying mass radius relation to get radius of the secondary
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)

    #will need the separation to stay above the Roche Lobe separation for both the primary and secondary.
    #therefore, build a new arl that is the max of each at any given point, and do analysis with this.
    arl = np.zeros(np.size(arlM))
    for i in range(np.size(arlM)):
        arl[i] = np.max([arlM[i], arlm[i]])

    #The Nicholas guess and check method supreme, that probably has an actual name
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    large_increase = 5 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
    #at that respective time.
    medium_decrease = 0.5 #units of solar radii
    while np.min(arl_predpoints - arl) > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
     
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    small_increase = 0.01 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm



#Function for when the primary is evolving, continuous mass loss
def Minsep_full2(M, q, R, dm, dM, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, T = Minsep_details(M, q, R)
    
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R) #applying mass radius relation to get radius of the secondary
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
    
    #The Nicholas guess and check method supreme, that probably has an actual name
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    large_increase = 5 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
    #at that respective time.
    medium_decrease = 0.5 #units of solar radii
    while np.min(arl_predpoints - arl) > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
     
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    small_increase = 0.01 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm

  

#Function for when the secondary is evolving, continuous mass loss
def Minsep_full3(M, q, R, dm, dM, arl_points, phase4, end_time, h):
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    
    #getting roche lobe separation and period for the secondary
    arlm, T = Minsep_details(m, qm, R)
    
    #getting roche lobe separation and period for the primary
    RM = np.copy(R/m[0]) #R is for the secondary here, so to get the radius of the primary, undo mass radius relation
    arlM, TM = Minsep_details(M, q, RM)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
    
    ams =  arl_points[-1] #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, T, dm, dM, end_time, h)
    
    #if phase4 == 0, then end here. otherwise, adjust the separation to prevent mass transfer
    indicator = 0 #change to 1 if adjustments happen
    if phase4 == 1:
        #The Nicholas guess and check method supreme, that probably has an actual name
        trigger_phase_2 = 0 #without this, phase 2 always activates, so with this it only activates after phase 1
        
        #phase 1
        #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
        #at that respective time.
        large_increase = 5 #units of solar radii
        while np.min(arl_predpoints - arl) < 0:
            ams += large_increase
            arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, T, dm, dM, end_time, h)
            trigger_phase_2 = 1
        
        #phase 2
        #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
        #at that respective time.
        if trigger_phase_2 == 1:
            medium_decrease = 0.5 #units of solar radii
            while np.min(arl_predpoints - arl) > 0:
                ams -= medium_decrease
                arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, T, dm, dM, end_time, h)
         
        #phase 3
        #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
        #at that respective time.
        small_increase = 0.01 #units of solar radii
        while np.min(arl_predpoints - arl) < 0:
            indicator = 1 #adjustments have happened if this loop is activated
            ams += small_increase
            arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, T, dm, dM, end_time, h)
    
    return ams, arl_predpoints, omegapredpointsM, omegapredpointsm, indicator



#Function that adjusts the primary's final separation to match up with the secondary's starting separation,
#if the secondary was adjusted to prevent a mass transfer event from occuring, continuous mass loss
def Minsep_full4(M, q, R, dm, dM, final_sep, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, T = Minsep_details(M, q, R)
    
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R)
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
    
    #The Nicholas guess and check method supreme, that probably has an actual name (modified to fit a final value)
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the starting separation
    #for the secondary.
    large_increase = 5 #units of solar radii
    while arl_predpoints[-1] - final_sep < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the starting separation
    #for the secondary.
    medium_decrease = 0.5 #units of solar radii
    while arl_predpoints[-1] - final_sep > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
     
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the starting separation
    #for the secondary.
    small_increase = 0.01 #units of solar radii
    while arl_predpoints[-1] - final_sep < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, T, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm



#Function for when the primary and secondary evolve at the same rate, instant mass loss at the end
def helium_flash(M, q, R, M_new, dm, dM, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, TM = Minsep_details(M, q, R)
   
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R) #applying mass radius relation to get radius of the secondary
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)
    
    #will need the separation to stay above the Roche Lobe separation for both the primary and secondary.
    #therefore, build a new arl that is the max of each at any given point, and do analysis with this.
    arl = np.zeros(np.size(arlM))
    for i in range(np.size(arlM)):
        arl[i] = np.max([arlM[i], arlm[i]])
    
    #The Nicholas guess and check method supreme, that probably has an actual name
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    large_increase = 5 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
    #at that respective time.
    medium_decrease = 0.5 #units of solar radii
    while np.min(arl_predpoints - arl) > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
     
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    small_increase = 0.01 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsep(ams, M, q, R, Rm, TM, Tm, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #use M_new to determine how much mass is lost
    mass_lost = abs(M_new - m[-1] - M[-1]) 
    
    #applying instant helium flash
    mass = mass_lost/(m[-1]+M[-1]) #defining for convinience, assuming m+M is pre mass loss
    arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
    arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
 
    #increase size of omegapoints to make later code work, can add the actual new omega later if needed
    omegapredpointsM = np.hstack([omegapredpointsM, np.array([omegapredpointsM[-1]])])
    omegapredpointsm = np.hstack([omegapredpointsm, np.array([omegapredpointsm[-1]])])
    
    #eccentricity
    e = (1 - (1 - epred**2)*((1 - ((2*mass)/(1 - epred)))/((1-mass)**2)))**0.5
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm, e



#Function for when the primary is evolving, instant mass loss at the end
def helium_flash2(M, q, R, M_new, dm, dM, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, TM = Minsep_details(M, q, R)

    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R) #applying mass radius relation to get radius of the secondary
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
    
    #The Nicholas guess and check method supreme, that probably has an actual name
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    large_increase = 5 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
    #at that respective time.
    medium_decrease = 0.5 #units of solar radii
    while np.min(arl_predpoints - arl) > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
     
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
    #at that respective time.
    small_increase = 0.01 #units of solar radii
    while np.min(arl_predpoints - arl) < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #use M_new to determine how much mass is lost
    mass_lost = abs(M_new - m[-1] - M[-1]) 
    
    #applying instant helium flash
    mass = mass_lost/(m[-1]+M[-1]) #defining for convinience, assuming m+M is pre mass loss
    arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
    arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
 
    #increase size of omegapoints to make later code work, can add the actual new omega later if needed
    omegapredpointsM = np.hstack([omegapredpointsM, np.array([omegapredpointsM[-1]])])
    omegapredpointsm = np.hstack([omegapredpointsm, np.array([omegapredpointsm[-1]])])
    
    #eccentricity
    e = (1 - (1 - epred**2)*((1 - ((2*mass)/(1 - epred)))/((1-mass)**2)))**0.5
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm, e



#Function for when the secondary is evolving, instant mass loss at the end
def helium_flash3(M, q, R, M_new, dm, dM, starting_sep, phase4, end_time, h):
    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, R)
    
    #getting roche lobe separation and period for the primary
    RM = np.copy(R/m[0]) #R is for the secondary here, so to get the radius of the primary, undo mass radius relation
    arlM, TM = Minsep_details(M, q, RM)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
        
    ams =  starting_sep[-1] #solar radii

    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, Tm, dm, dM, end_time, h)
    
    #if phase4 == 0, then end here. otherwise, adjust the separation to prevent mass transfer
    indicator = 0 #change to 1 if adjustments happen
    if phase4 == 1:
        #The Nicholas guess and check method supreme, that probably has an actual name
        trigger_phase_2 = 0 #without this, phase 2 always activates, so with this it only activates after phase 1
        
        #phase 1
        #increase ams by large increments until every value of the separation is greater than the Roche Lobe Separation
        #at that respective time.
        large_increase = 5 #units of solar radii
        while np.min(arl_predpoints - arl) < 0:
            ams += large_increase
            arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, Tm, dm, dM, end_time, h)
            trigger_phase_2 = 1
        
        #phase 2
        #decrease ams by medium increments until one value of the separation is less than the Roche Lobe Separation 
        #at that respective time.
        #whether or not to trigger phase 2
        if trigger_phase_2 == 1:
            medium_decrease = 0.5 #units of solar radii
            while np.min(arl_predpoints - arl) > 0:
                ams -= medium_decrease
                arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, Tm, dm, dM, end_time, h)
        
        #phase 3
        #increase ams by small increments until every value of the separation is greater than the Roche Lobe Separation
        #at that respective time.
        small_increase = 0.01 #units of solar radii
        while np.min(arl_predpoints - arl) < 0:
            indicator = 1
            ams += small_increase
            arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepm(ams, M, q, R, Tm, dm, dM, end_time, h)
    
    #use M_new to determine how much mass is lost
    mass_lost = abs(M_new - m[-1] - M[-1]) 
    
    #applying instant helium flash
    mass = mass_lost/(m[-1]+M[-1]) #defining for convinience, assuming m+M is pre mass loss
    arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
    arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
 
    #increase size of omegapoints to make later code work, can add the actual new omega later if needed
    omegapredpointsM = np.hstack([omegapredpointsM, np.array([omegapredpointsM[-1]])])
    omegapredpointsm = np.hstack([omegapredpointsm, np.array([omegapredpointsm[-1]])])
    
    #eccentricity
    e = (1 - (1 - epred**2)*((1 - ((2*mass)/(1 - epred)))/((1-mass)**2)))**0.5
    
    return ams, arl_predpoints, omegapredpointsM, omegapredpointsm, e, indicator



#Function that adjusts the primary's final separation to match up with the secondary's starting separation,
#if the secondary was adjusted to prevent a mass transfer event from occuring, instant mass loss at the end
def helium_flash4(M, q, R, M_new, dm, dM, final_sep, end_time, h):
    #getting roche lobe separation and period for the primary
    arlM, TM = Minsep_details(M, q, R)

    m = q*M #mass of the secondary
    qm = np.copy(1/q) #the mass ratio with the secondary in the numerator
    Rm = np.copy(m[0]*R) #applying mass radius relation to get radius of the secondary
    
    #getting roche lobe separation and period for the secondary
    arlm, Tm = Minsep_details(m, qm, Rm)
    
    #this will make sure no roche lobe overflow happens for the secondary as well, but only if its mass is
    #greater than 0.5, as masses less than this wont be RR Lyrae
    if m[0] < 0.5:
        arl = np.copy(arlM)
    else:
        arl = np.zeros(np.size(arlM))
        for i in range(np.size(arlM)):
            arl[i] = np.max([arlM[i], arlm[i]])
    
    #Need to apply mass loss before doing the adjustment, since the final endpoint is the reference here
    #The Nicholas guess and check method supreme, that probably has an actual name
    ams =  100 #solar radii
    
    arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
    pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
    
    #use M_new to determine how much mass is lost
    mass_lost = abs(M_new - m[-1] - M[-1]) 
    
    #applying instant helium flash
    mass = mass_lost/(m[-1]+M[-1]) #defining for convinience, assuming m+M is pre mass loss
    arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
    arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
    
    #phase 1
    #increase ams by large increments until every value of the separation is greater than the starting separation
    #for the secondary.
    large_increase = 5 #units of solar radii
    while arl_predpoints[-1] - final_sep < 0:
        ams += large_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
        
        #applying instant helium flash
        arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
        arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
        
    #phase 2
    #decrease ams by medium increments until one value of the separation is less than the starting separation
    #for the secondary.
    medium_decrease = 0.5 #units of solar radii
    while arl_predpoints[-1] - final_sep > 0:
        ams -= medium_decrease
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
        
        #applying instant helium flash
        arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
        arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
        
    #phase 3
    #increase ams by small increments until every value of the separation is greater than the starting separation
    #for the secondary.
    small_increase = 0.01 #units of solar radii
    while arl_predpoints[-1] - final_sep < 0:
        ams += small_increase
        arl_pred, arl_predpoints, omegapredpointsM, epred, omegapredpointsm = minsepM(ams, M, q, R, TM, dm, dM, end_time, h)
        pred = np.argmin(arl_predpoints) #index of the minimum value of arlpred
        
        #applying instant helium flash
        arl_pred *= (1/2)*((1-mass)/(1/2 - ((mass)/(1-epred))))
        arl_predpoints = np.hstack([arl_predpoints, np.array([arl_pred])])
    
    #increase size of omegapoints to make later code work, can add the actual new omega later if needed
    omegapredpointsM = np.hstack([omegapredpointsM, np.array([omegapredpointsM[-1]])])
    omegapredpointsm = np.hstack([omegapredpointsm, np.array([omegapredpointsm[-1]])])
    
    #eccentricity
    e = (1 - (1 - epred**2)*((1 - ((2*mass)/(1 - epred)))/((1-mass)**2)))**0.5
    
    return ams, arl[pred], arl_pred, arl_predpoints, omegapredpointsM, omegapredpointsm, e


  
'''
Functions that consider systems of ODEs with changing binding energy but no tidal interactions 
'''   
#System of ODEs when there is no tidal interaction, only a change in binding energy   
def fbinding(r, M, q, R, dm, dM):
    a = r[0]
    e = r[2]
    m = q*M #mass of the secondary

    #equations with only mass loss
    #separation due to binding energy
    fa = - ((1+e)/(1-e))*((a)/(m+M))*(dm+dM)
    
    #the rotational angular velocity for the primary
    fomegaM = 0
    
    #the rotational angular velocity for the secondary
    fomegam = 0
    
    #eccentricity
    fe = 0
    return np.array([fa, fomegaM, fe, fomegam])



#Solves the fbinding system of ODEs
def binding_energy(ams, M, q, R, dm, dM, end_time, h):
    r = np.array([ams, 0.0, 0.0, 0.0]) #initial omega is 0 by assumption, for both M and m
    time = np.arange(0.0, end_time, h) #time array for RK4 method
    
    #making lists and adding initial values to them
    Rpoints = []
    omegaMpoints = []
    omegampoints = []
    Rpoints.append(ams)
    omegaMpoints.append(0.0)
    omegampoints.append(0.0)

    #RK4 method
    for t in range(len(time)):
        t = t+1 #t shifted forward so that the correct values are called for a given r.
        if r[0] > R: #program stops if theres a collision, defined to be the separation less than the radius of the primary.
           k1 = h*fbinding(r, M[t], q[t], R, dm[t], dM[t])
           k2 = h*fbinding(r + 0.5*k1, M[t], q[t], R, dm[t], dM[t])
           k3 = h*fbinding(r + 0.5*k2, M[t], q[t], R, dm[t], dM[t])
           k4 = h*fbinding(r + k3, M[t], q[t], R, dm[t], dM[t])
           r += (k1 + 2*k2 + 2*k3 + k4)/6 
        else:
            r = 0.0, 0.0, 0.0, 0.0
            
        #adding most recent values to lists
        Rpoints.append(r[0])
        omegaMpoints.append(r[1])
        omegampoints.append(r[3])
    #turning lists into arrays
    Rpoints = np.array(Rpoints)
    omegaMpoints = np.array(omegaMpoints)
    omegampoints = np.array(omegampoints)

    #python thinks nan is a min, so remove these
    for i in range(np.size(Rpoints)):
        if np.isnan(Rpoints[i]) == True:
            Rpoints[i] = 0
            
    #return the final separation and separation array
    return r[0], Rpoints



'''
Now making the functions that combine the above functions into useful data
'''
#list of time dependent variables to load
#R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
#dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb



'''
Instant Helium Flash
'''
#Primary Evolution
def Primary_Instant(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                    dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #arrays to store data
    ams_list1 = np.zeros(size1)
    arl_list1 = np.zeros(size1)
    arl_pred_list1 = np.zeros(size1)
    e_list1 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            (ams_list1[i], arl_list1[i], arl_pred_list1[i], arl_predpoints1_0, 
             omegapredpointsM1_0, omegapredpointsm1_0, e_list1[i]) \
            = helium_flash2(M1_a, q_range1[i]*q1_a, R, new_mass_a[i], q_range1[i]*dm_a, dM1_a, end_time, h)
            size2 = len(arl_predpoints1_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array1 = np.zeros((size1, size2))
            omegapredpointsM_array1 = np.zeros((size1, size2))
            omegapredpointsm_array1 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array1[i] = arl_predpoints1_0
            omegapredpointsM_array1[i] = omegapredpointsM1_0
            omegapredpointsm_array1[i] = omegapredpointsm1_0
            print(i) #indicates that this function has started
        else:
            (ams_list1[i], arl_list1[i], arl_pred_list1[i], arl_predpoints_array1[i], 
             omegapredpointsM_array1[i], omegapredpointsM_array1[i], e_list1[i]) \
            = helium_flash2(M1_a, q_range1[i]*q1_a, R, new_mass_a[i], q_range1[i]*dm_a, dM1_a, end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
        
    #giving these values their own name so that the starting one can be reused later
    ams_list1_prem = np.copy(ams_list1)
    arl_list1_prem = np.copy(arl_list1)
    arl_pred_list1_prem = np.copy(arl_pred_list1)
    arl_predpoints_array1_prem = np.copy(arl_predpoints_array1)
    omegapredpointsM_array1_prem = np.copy(omegapredpointsM_array1)
    omegapredpointsm_array1_prem = np.copy(omegapredpointsm_array1)
    e_list1_prem = np.copy(e_list1)
    
    return (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
            omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem)
        


#Secondary Evolution
def Secondary_Instant(phase4, new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                      arl_predpoints_array1_prem, omegapredpointsM_array1_prem, 
                      omegapredpointsm_array1_prem, e_list1_prem, 
                      R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                      dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):    
    #arrays to store data
    ams_list1 = np.zeros(size1)
    arl_list1 = np.zeros(size1)
    arl_pred_list1 = np.zeros(size1)
    e_list1 = np.zeros(size1)
    alert1 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            ams_list1[i], arl_predpoints1_0, omegapredpointsM1_0, omegapredpointsm1_0, e_list1[i], alert1[i] \
            = helium_flash3(M_b, q_range1[i]*qb, q_range1[i]*R, new_mass_a[i], 
                            q_range1[i]*dm_a, dM_b, arl_predpoints_array1_prem[i], phase4, end_time, h)
            
            #adjusts primary separation to match with secondary separation if the latter was adjusted
            if alert1[i] == 1 and phase4 == 1:
                (ams_list1_prem[i], arl_list1_prem[i], arl_pred_list1_prem[i], arl_predpoints_array1_prem[i], 
                 omegapredpointsM_array1_prem[i], omegapredpointsm_array1_prem[i], e_list1_prem[i]) \
                = helium_flash4(M1_a, q_range1[i]*q1_a, R, new_mass_a[i], q_range1[i]*dm_a, 
                                dM1_a, ams_list1[i], end_time, h)
            size2 = len(arl_predpoints1_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array1 = np.zeros((size1, size2))
            omegapredpointsM_array1 = np.zeros((size1, size2))
            omegapredpointsm_array1 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array1[i] = arl_predpoints1_0
            omegapredpointsM_array1[i] = omegapredpointsM1_0
            omegapredpointsm_array1[i] = omegapredpointsm1_0
            print(i) #indicates that this function has started
        else:
            if i<79: #uses constant m for the lower part and changing m for the higher part
                (ams_list1[i], arl_predpoints_array1[i], omegapredpointsM_array1[i], 
                 omegapredpointsm_array1[i], e_list1[i], alert1[i]) \
                = helium_flash3(M_b, q_range1[i]*qb, q_range1[i]*R, new_mass_a[i], 
                                q_range1[i]*dm_a, dM_b, arl_predpoints_array1_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert1[i] == 1 and phase4 == 1:
                    (ams_list1_prem[i], arl_list1_prem[i], arl_pred_list1_prem[i], arl_predpoints_array1_prem[i], 
                     omegapredpointsM_array1_prem[i], omegapredpointsm_array1_prem[i], e_list1_prem[i]) \
                    = helium_flash4(M1_a, q_range1[i]*q1_a, R, new_mass_a[i], q_range1[i]*dm_a, 
                                    dM1_a, ams_list1[i], end_time, h)
            else:
                (ams_list1[i], arl_predpoints_array1[i], omegapredpointsM_array1[i], 
                 omegapredpointsm_array1[i], e_list1[i], alert1[i]) \
                = helium_flash3(M_b, q_range1[i]*q1_b, q_range1[i]*R, new_mass_b[i], 
                                q_range1[i]*dm1_b, dM_b, arl_predpoints_array1_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert1[i] == 1 and phase4 == 1:
                    (ams_list1_prem[i], arl_list1_prem[i], arl_pred_list1_prem[i], arl_predpoints_array1_prem[i], 
                     omegapredpointsM_array1_prem[i], omegapredpointsm_array1_prem[i], e_list1_prem[i]) \
                    = helium_flash4(M1_a, q_range1[i]*q1_a, R, new_mass_a[i], 
                                    q_range1[i]*dm_a, dM1_a, ams_list1[i], end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
                
    return (ams_list1, arl_list1, arl_pred_list1, arl_predpoints_array1, 
            omegapredpointsM_array1, omegapredpointsm_array1, e_list1)



#Function that overwrites primary and secondary evolution for large q when the primary and secondary are evolving at
#the same time
def Overwrite_Instant(new_mass_a, new_mass_b, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, 
                      arl_predpoints_array1_prem, omegapredpointsM_array1_prem, ams_list1, arl_list1, 
                      arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1,
                      omegapredpointsm_array1_prem, e_list1_prem, 
                      R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                      dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    for i in range(size1):
        if i > size1 - 4: #only overwrites large q
            (ams_list1[i], arl_list1[i], arl_pred_list1[i], arl_predpoints_array1[i], omegapredpointsM_array1[i], 
                 omegapredpointsm_array1[i], e_list1[i]) \
                = helium_flash(M1_a, q_range1[i]*qb, R, new_mass_b[i], 
                                q_range1[i]*dm1_b, dM1_a, end_time, h)
            #primary and secondary separation are the same here
            ams_list1_prem[i] = np.copy(ams_list1[i])
            arl_list1_prem[i] = np.copy(arl_list1[i])
            arl_pred_list1_prem[i] = np.copy(arl_pred_list1[i])
            arl_predpoints_array1_prem[i] = np.copy(arl_predpoints_array1[i])
            omegapredpointsM_array1_prem[i] = np.copy(omegapredpointsM_array1[i])
            omegapredpointsm_array1_prem[i] = np.copy(omegapredpointsm_array1[i])
            e_list1_prem[i] = np.copy(e_list1[i])

            

#Primary Evolution, allows for varying radius and mass loss
def Primary_Instant_Custom(new_mass_a, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, 
                           m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b,
                           qb, end_time, h):
    #size of arrays
    size3 = len(R)
    size4 = len(new_mass_a[0])
    
    #arrays to store data
    ams_list1 = np.zeros((size1, size3, size4))
    arl_list1 = np.zeros((size1, size3, size4))
    arl_pred_list1 = np.zeros((size1, size3, size4))
    e_list1 = np.zeros((size1, size3, size4))
    
    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                #for the 1st run, build some lists from scratch so their size can be determined and used in the future
                if i == 0 and j == 0 and k == 0:
                    (ams_list1[i,j,k], arl_list1[i,j,k], arl_pred_list1[i,j,k], arl_predpoints1_0, 
                     omegapredpointsM1_0, omegapredpointsm1_0, e_list1[i,j,k]) \
                    = helium_flash2(M1_a[k], q_range1[i]*q1_a[k], R[j], 
                                    new_mass_a[i,k], q_range1[i]*dm_a[k], dM1_a[k], end_time, h)
                    size2 = len(arl_predpoints1_0)
                    
                    #since the data is already a list, 2d arrays of the appropriate size are created for storing data
                    arl_predpoints_array1 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsM_array1 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsm_array1 = np.zeros((size1, size3, size4, size2))
                    
                    #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
                    arl_predpoints_array1[i,j,k] = arl_predpoints1_0
                    omegapredpointsM_array1[i,j,k] = omegapredpointsM1_0
                    omegapredpointsm_array1[i,j,k] = omegapredpointsm1_0
                    print(i, j, k) #indicates that this function has started
                else:
                    (ams_list1[i,j,k], arl_list1[i,j,k], arl_pred_list1[i,j,k], arl_predpoints_array1[i,j,k], 
                     omegapredpointsM_array1[i,j,k], omegapredpointsM_array1[i,j,k], e_list1[i,j,k]) \
                    = helium_flash2(M1_a[k], q_range1[i]*q1_a[k], R[j], new_mass_a[i,k], q_range1[i]*dm_a[k], 
                                    dM1_a[k], end_time, h)
                    if i%2 == 0 and j%2 == 0 and k %2 == 0: #prints every even entry, as a means of checking progress
                        print(i, j, k)
        
    #giving these values their own name so that the starting one can be reused later
    ams_list1_prem = np.copy(ams_list1)
    arl_list1_prem = np.copy(arl_list1)
    arl_pred_list1_prem = np.copy(arl_pred_list1)
    arl_predpoints_array1_prem = np.copy(arl_predpoints_array1)
    omegapredpointsM_array1_prem = np.copy(omegapredpointsM_array1)
    omegapredpointsm_array1_prem = np.copy(omegapredpointsm_array1)
    e_list1_prem = np.copy(e_list1)
    
    return (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
            omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem)



#Binding energy only evolution for the primary
def Primary_Instant_Binding(ams_list1_prem, mass_lost, 
                            R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                            dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                            q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):        
    #defining for convinience, assuming m+M is pre mass loss   
    mass1_a = mass_lost/(m_a[-1]*q_range1 + M1_a[-1]*np.ones(size1))
    
    #arrays to store data
    ams_binding1 = np.ones(size1)
    
    for i in range(size1):   
        ams_binding1[i] = (ams_list1_prem[i]/2)*((1-mass1_a[i])/(1/2 - (mass1_a[i])))
    
    #giving these values their own name so that the starting one can be reused later
    ams_binding1_prem = np.copy(ams_binding1)
    
    return ams_binding1_prem



#Binding energy only evolution for the primary and secondary
def Secondary_Instant_Binding(ams_list1_prem, ams_binding1_prem, mass_lost, 
                              R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                              dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                              q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):      
    #defining for convinience, assuming m+M is pre mass loss  
    mass2_b = mass_lost/(m1_b[-1]*q_range1 + M_b[-1]*np.ones(size1))
    
    #arrays to store data
    ams_binding1 = np.ones(size1)
    
    for i in range(size1):   
        if i<79: #uses constant m for the lower part and changing m for the higher part
            ams_binding1[i] = np.copy(ams_binding1_prem[i])
        else:
            ams_binding1[i] = (ams_binding1_prem[i]/2)*((1-mass2_b[i])/(1/2 - (mass2_b[i])))
            
    return ams_binding1



#Function that overwrites primary and secondary Binding energy for large q when the primary and secondary are evolving 
#at the same time
def Overwrite_Instant_Binding(ams_list1_prem, ams_list1, ams_binding1_prem, ams_binding1, mass_lost, 
                            R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                            dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                            q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):        
    #defining for convinience, assuming m+M is pre mass loss   
    mass1_a = mass_lost/(m1_b[-1]*q_range1 + M1_a[-1]*np.ones(size1))
    
    for i in range(size1):  
        if i > size1 - 4: #only overwrites large q
            ams_binding1[i] = (ams_list1_prem[i]/2)*((1-mass1_a[i])/(1/2 - (mass1_a[i])))
    
        #primary and secondary separation are the same here
        ams_binding1_prem[i] = np.copy(ams_binding1[i])
        
        
        
#Binding energy only evolution for the primary, allows for varying radius and mass loss
def Primary_Instant_Binding_Custom(ams_list1_prem, mass_lost, 
                            R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                            dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                            q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):  
    #size of arrays
    size3 = len(R)
    size4 = len(mass_lost)
      
    #defining for convinience, assuming m+M is pre mass loss   
    mass1_a = np.ones((size1, size4))
    for k in range(size4):
        mass1_a[:,k] = mass_lost[k]/(m_a[k,-1]*q_range1 + M1_a[k,-1]*np.ones(size1))
    
    #arrays to store data
    ams_binding1 = np.ones((size1, size3, size4))
    
    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                    ams_binding1[i,j,k] = (ams_list1_prem[i,j,k]/2)*((1 - mass1_a[i,k])/(1/2 - (mass1_a[i,k])))
    
    #giving these values their own name so that the starting one can be reused later
    ams_binding1_prem = np.copy(ams_binding1)
    
    return ams_binding1_prem

    


'''
Slow Helium Flash
'''
#Primary Evolution
def Primary_Slow(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):   
    #arrays to store data
    ams_list2 = np.zeros(size1)
    arl_list2 = np.zeros(size1)
    arl_pred_list2 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            (ams_list2[i], arl_list2[i], arl_pred_list2[i], arl_predpoints2_0,
             omegapredpointsM2_0, omegapredpointsm2_0) \
            = Minsep_full2(M2_a, q_range1[i]*q2_a, R, q_range1[i]*dm_a, dM2_a, end_time, h)
            size2 = len(arl_predpoints2_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array2 = np.zeros((size1, size2))
            omegapredpointsM_array2 = np.zeros((size1, size2))
            omegapredpointsm_array2 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array2[i] = arl_predpoints2_0
            omegapredpointsM_array2[i] = omegapredpointsM2_0
            omegapredpointsm_array2[i] = omegapredpointsm2_0
            print(i) #indicates that this function has started
        else:
            (ams_list2[i], arl_list2[i], arl_pred_list2[i], arl_predpoints_array2[i], 
             omegapredpointsM_array2[i], omegapredpointsm_array2[i]) \
            = Minsep_full2(M2_a, q_range1[i]*q2_a, R, q_range1[i]*dm_a, dM2_a, end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
    
    #giving these values their own name so that the starting one can be reused later
    ams_list2_prem = np.copy(ams_list2)
    arl_list2_prem = np.copy(arl_list2)
    arl_pred_list2_prem = np.copy(arl_pred_list2)
    arl_predpoints_array2_prem = np.copy(arl_predpoints_array2)
    omegapredpointsM_array2_prem = np.copy(omegapredpointsM_array2)
    omegapredpointsm_array2_prem = np.copy(omegapredpointsm_array2)
    
    return (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
            omegapredpointsM_array2_prem, omegapredpointsm_array2_prem)



#Secondary Evolution
def Secondary_Slow(phase4, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
                   arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
                   R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                   dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #arrays to store data
    ams_list2 = np.zeros(size1)
    arl_list2 = np.zeros(size1)
    arl_pred_list2 = np.zeros(size1)
    alert2 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            ams_list2[i], arl_predpoints2_0, omegapredpointsM2_0, omegapredpointsm2_0, alert2[i] \
            = Minsep_full3(M_b, q_range1[i]*qb, q_range1[i]*R, q_range1[i]*dm_a, 
                           dM_b, arl_predpoints_array2_prem[i], phase4, end_time, h)
            
            #adjusts primary separation to match with secondary separation if the latter was adjusted
            if alert2[i] == 1 and phase4 == 1:
                (ams_list2_prem[i], arl_list2_prem[i], arl_pred_list2_prem[i], arl_predpoints_array2_prem[i], 
                 omegapredpointsM_array2_prem[i], omegapredpointsm_array2_prem[i]) \
                = Minsep_full4(M2_a, q_range1[i]*q2_a, R, q_range1[i]*dm_a, dM2_a, ams_list2[i])
            size2 = len(arl_predpoints2_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array2 = np.zeros((size1, size2))
            omegapredpointsM_array2 = np.zeros((size1, size2))
            omegapredpointsm_array2 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array2[i] = arl_predpoints2_0
            omegapredpointsM_array2[i] = omegapredpointsM2_0
            omegapredpointsm_array2[i] = omegapredpointsm2_0
            print(i) #indicates that this function has started
        else:
            if i<79: #uses constant m for the lower part and changing m for the higher part
                (ams_list2[i], arl_predpoints_array2[i], omegapredpointsM_array2[i], 
                 omegapredpointsm_array2[i], alert2[i]) \
                = Minsep_full3(M_b, q_range1[i]*qb, q_range1[i]*R, q_range1[i]*dm_a, 
                               dM_b, arl_predpoints_array2_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert2[i] == 1 and phase4 == 1:
                    (ams_list2_prem[i], arl_list2_prem[i], arl_pred_list2_prem[i], arl_predpoints_array2_prem[i], 
                     omegapredpointsM_array2_prem[i], omegapredpointsm_array2_prem[i]) \
                    = Minsep_full4(M2_a, q_range1[i]*q2_a, R, q_range1[i]*dm_a, dM2_a, ams_list2[i], end_time, h)
            else:
                (ams_list2[i], arl_predpoints_array2[i], omegapredpointsM_array2[i], 
                 omegapredpointsm_array2[i], alert2[i]) \
                = Minsep_full3(M_b, q_range1[i]*q2_b, q_range1[i]*R, q_range1[i]*dm2_b, 
                               dM_b, arl_predpoints_array2_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert2[i] == 1 and phase4 == 1:
                    (ams_list2_prem[i], arl_list2_prem[i], arl_pred_list2_prem[i], arl_predpoints_array2_prem[i], 
                     omegapredpointsM_array2_prem[i], omegapredpointsm_array2_prem[i]) \
                    = Minsep_full4(M2_a, q_range1[i]*q2_a, R, q_range1[i]*dm_a, dM2_a, ams_list2[i], end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
                
    return (ams_list2, arl_list2, arl_pred_list2, arl_predpoints_array2, 
            omegapredpointsM_array2, omegapredpointsm_array2)



#Function that overwrites primary and secondary evolution for large q when the primary and secondary are evolving at
#the same time
def Overwrite_Slow(ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
                      arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, 
                      ams_list2, arl_list2, 
                      arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2,
                      R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                      dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    for i in range(size1):
        if i > size1 - 4: #only overwrites large q
            (ams_list2[i], arl_list2[i], arl_pred_list2[i], arl_predpoints_array2[i], omegapredpointsM_array2[i], 
                 omegapredpointsm_array2[i]) \
                = Minsep_full(M2_a, q_range1[i]*qb, R, 
                                q_range1[i]*dm2_b, dM2_a, end_time, h)
            #primary and secondary separation are the same here
            ams_list2_prem[i] = np.copy(ams_list2[i])
            arl_list2_prem[i] = np.copy(arl_list2[i])
            arl_pred_list2_prem[i] = np.copy(arl_pred_list2[i])
            arl_predpoints_array2_prem[i] = np.copy(arl_predpoints_array2[i])
            omegapredpointsM_array2_prem[i] = np.copy(omegapredpointsM_array2[i])
            omegapredpointsm_array2_prem[i] = np.copy(omegapredpointsm_array2[i])
            
            
            
#Primary Evolution, allows for varying radius and mass loss
def Primary_Slow_Custom(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #size of arrays
    size3 = len(R)
    size4 = len(M1_a[:,0])
    
    #arrays to store data
    ams_list2 = np.zeros((size1, size3, size4))
    arl_list2 = np.zeros((size1, size3, size4))
    arl_pred_list2 = np.zeros((size1, size3, size4))
    
    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                #for the 1st run, build some lists from scratch so their size can be determined and used in the future
                if i == 0 and j == 0 and k == 0:
                    (ams_list2[i,j,k], arl_list2[i,j,k], arl_pred_list2[i,j,k], arl_predpoints2_0,
                     omegapredpointsM2_0, omegapredpointsm2_0) \
                    = Minsep_full2(M2_a[k], q_range1[i]*q2_a[k], R[j], q_range1[i]*dm_a[k], dM2_a[k], end_time, h)
                    size2 = len(arl_predpoints2_0)
                    
                    #since the data is already a list, 2d arrays of the appropriate size are created for storing data
                    arl_predpoints_array2 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsM_array2 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsm_array2 = np.zeros((size1, size3, size4, size2))
                    
                    #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
                    arl_predpoints_array2[i,j,k] = arl_predpoints2_0
                    omegapredpointsM_array2[i,j,k] = omegapredpointsM2_0
                    omegapredpointsm_array2[i,j,k] = omegapredpointsm2_0
                    print(i, j, k) #indicates that this function has started
                else:
                    (ams_list2[i,j,k], arl_list2[i,j,k], arl_pred_list2[i,j,k], arl_predpoints_array2[i,j,k], 
                     omegapredpointsM_array2[i,j,k], omegapredpointsm_array2[i,j,k]) \
                    = Minsep_full2(M2_a[k], q_range1[i]*q2_a[k], R[j], q_range1[i]*dm_a[k], dM2_a[k], end_time, h)
                    if i%2 == 0 and j%2 == 0 and k%2 == 0: #prints every even entry, as a means of checking progress
                        print(i, j, k)
    
    #giving these values their own name so that the starting one can be reused later
    ams_list2_prem = np.copy(ams_list2)
    arl_list2_prem = np.copy(arl_list2)
    arl_pred_list2_prem = np.copy(arl_pred_list2)
    arl_predpoints_array2_prem = np.copy(arl_predpoints_array2)
    omegapredpointsM_array2_prem = np.copy(omegapredpointsM_array2)
    omegapredpointsm_array2_prem = np.copy(omegapredpointsm_array2)
    
    return (ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
            omegapredpointsM_array2_prem, omegapredpointsm_array2_prem)



#Binding energy only evolution for the primary
def Primary_Slow_Binding(ams_list2_prem, 
                         R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                         dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):       
    #array to store data
    ams_binding2 = np.zeros(size1)

    for i in range(size1):
        if i == 0:
            ams_binding2[i], ams_points_binding2_0 = binding_energy(ams_list2_prem[i], M2_a, q_range1[i]*q2_a, 
                        R, q_range1[i]*dm_a, dM2_a, end_time, h)
            size2 = len(ams_points_binding2_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            ams_points_binding2 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            ams_points_binding2[i] = ams_points_binding2_0
        else:
            ams_binding2[i], ams_points_binding2[i] = binding_energy(ams_list2_prem[i], M2_a, q_range1[i]*q2_a, 
                        R, q_range1[i]*dm_a, dM2_a, end_time, h)
    
    #giving these values their own name so that the starting one can be reused later     
    ams_binding2_prem = np.copy(ams_binding2)
    ams_points_binding2_prem = np.copy(ams_points_binding2)
    
    return ams_binding2_prem, ams_points_binding2_prem



#Binding energy only evolution for the primary and secondary
def Secondary_Slow_Binding(ams_binding2_prem, 
                           R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                           dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                           q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #array to store data
    ams_binding2 = np.zeros(size1)
    
    for i in range(size1):
        if i == 0:
            ams_binding2[i], ams_points_binding2_0 = binding_energy(ams_binding2_prem[i], M_b, q_range1[i]*qb, 
                        q_range1[i]*R, q_range1[i]*dm_a, dM_b, end_time, h)
            size2 = len(ams_points_binding2_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            ams_points_binding2 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            ams_points_binding2[i] = ams_points_binding2_0
        else:
            if i<79: #uses constant m for the lower part and changing m for the higher part
                ams_binding2[i], ams_points_binding2[i] = binding_energy(ams_binding2_prem[i], M_b, q_range1[i]*qb, 
                            q_range1[i]*R, q_range1[i]*dm_a, dM_b, end_time, h)
            else:
                ams_binding2[i], ams_points_binding2[i] = binding_energy(ams_binding2_prem[i], M_b, q_range1[i]*q2_b, 
                            q_range1[i]*R, q_range1[i]*dm2_b, dM_b, end_time, h)
                
    return ams_binding2, ams_points_binding2



#Function that overwrites primary and secondary Binding energy for large q when the primary and secondary are evolving 
#at the same time
def Overwrite_Slow_Binding(ams_list2_prem, ams_list2, ams_binding2_prem, ams_points_binding2_prem, ams_binding2, 
                         ams_points_binding2, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, 
                         m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, 
                         q2_b, q3_b, qb, end_time, h):       
    for i in range(size1):
        if i > size1 - 4: #only overwrites large q
            ams_binding2[i], ams_points_binding2[i] = binding_energy(ams_list2_prem[i], M2_a, q_range1[i]*qb, 
                        R, q_range1[i]*dm2_b, dM2_a, end_time, h)
    
        #primary and secondary separation are the same here    
        ams_binding2_prem[i] = np.copy(ams_binding2[i])
        ams_points_binding2_prem[i] = np.copy(ams_points_binding2[i])
        
        
        
#Binding energy only evolution for the primary, allows for varying radius and mass loss
def Primary_Slow_Binding_Custom(ams_list2_prem, 
                         R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                         dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):       
    #size of arrays
    size3 = len(R)
    size4 = len(M1_a[:,0])
    
    #array to store data
    ams_binding2 = np.zeros((size1, size3, size4))

    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                if i == 0 and j == 0 and k == 0:
                    ams_binding2[i,j,k], ams_points_binding2_0 = binding_energy(ams_list2_prem[i,j,k], 
                                                                                M2_a[k], q_range1[i]*q2_a[k], 
                                R[j], q_range1[i]*dm_a[k], dM2_a[k], end_time, h)
                    size2 = len(ams_points_binding2_0)
                    
                    #since the data is already a list, 2d arrays of the appropriate size are created for storing data
                    ams_points_binding2 = np.zeros((size1, size3, size4, size2))
                    
                    #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
                    ams_points_binding2[i,j,k] = ams_points_binding2_0
                else:
                    ams_binding2[i,j,k], ams_points_binding2[i,j,k] = binding_energy(ams_list2_prem[i,j,k], M2_a[k], 
                                                                                     q_range1[i]*q2_a[k], 
                                R[j], q_range1[i]*dm_a[k], dM2_a[k], end_time, h)
    
    #giving these values their own name so that the starting one can be reused later     
    ams_binding2_prem = np.copy(ams_binding2)
    ams_points_binding2_prem = np.copy(ams_points_binding2)
    
    return ams_binding2_prem, ams_points_binding2_prem
        


'''        
Intermediate Helium Flash
'''
#Primary Evolution
def Primary_Intermediate(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                         dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #arrays to store data
    ams_list3 = np.zeros(size1)
    arl_list3 = np.zeros(size1)
    arl_pred_list3 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            (ams_list3[i], arl_list3[i], arl_pred_list3[i], arl_predpoints3_0, 
             omegapredpointsM3_0, omegapredpointsm3_0) \
            = Minsep_full2(M3_a, q_range1[i]*q3_a, R, q_range1[i]*dm_a, dM3_a, end_time, h)
            size2 = len(arl_predpoints3_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array3 = np.zeros((size1, size2))
            omegapredpointsM_array3 = np.zeros((size1, size2))
            omegapredpointsm_array3 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array3[i] = arl_predpoints3_0
            omegapredpointsM_array3[i] = omegapredpointsM3_0
            omegapredpointsm_array3[i] = omegapredpointsm3_0
            print(i) #indicates that this function has started
        else:
            (ams_list3[i], arl_list3[i], arl_pred_list3[i], arl_predpoints_array3[i], 
             omegapredpointsM_array3[i], omegapredpointsm_array3[i]) \
            = Minsep_full2(M3_a, q_range1[i]*q3_a, R, q_range1[i]*dm_a, dM3_a, end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
    
    #giving these values their own name so that the starting one can be reused later
    ams_list3_prem = np.copy(ams_list3)
    arl_list3_prem = np.copy(arl_list3)
    arl_pred_list3_prem = np.copy(arl_pred_list3)
    arl_predpoints_array3_prem = np.copy(arl_predpoints_array3)
    omegapredpointsM_array3_prem = np.copy(omegapredpointsM_array3)
    omegapredpointsm_array3_prem = np.copy(omegapredpointsm_array3)
    
    return (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
            omegapredpointsM_array3_prem, omegapredpointsm_array3_prem)



#Secondary Evolution
def Secondary_Intermediate(phase4, ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
                           omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, 
                           R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                           dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                           q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #arrays to store data
    ams_list3 = np.zeros(size1)
    arl_list3 = np.zeros(size1)
    arl_pred_list3 = np.zeros(size1)
    alert3 = np.zeros(size1)
    
    for i in range(size1):
        #for the 1st run, build some lists from scratch so their size can be determined and used in the future
        if i == 0:
            ams_list3[i], arl_predpoints3_0, omegapredpointsM3_0, omegapredpointsm3_0, alert3[i] \
            = Minsep_full3(M_b, q_range1[i]*qb, q_range1[i]*R, q_range1[i]*dm_a, dM_b, 
                           arl_predpoints_array3_prem[i], phase4, end_time, h)
            
            #adjusts primary separation to match with secondary separation if the latter was adjusted
            if alert3[i] == 1 and phase4 == 1:
                (ams_list3_prem[i], arl_list3_prem[i], arl_pred_list3_prem[i], arl_predpoints_array3_prem[i], 
                 omegapredpointsM_array3_prem[i], omegapredpointsm_array3_prem[i]) \
                = Minsep_full4(M3_a, q_range1[i]*q3_a, R, q_range1[i]*dm_a, dM3_a, ams_list3[i], end_time, h)
            size2 = len(arl_predpoints3_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            arl_predpoints_array3 = np.zeros((size1, size2))
            omegapredpointsM_array3 = np.zeros((size1, size2))
            omegapredpointsm_array3 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            arl_predpoints_array3[i] = arl_predpoints3_0
            omegapredpointsM_array3[i] = omegapredpointsM3_0
            omegapredpointsm_array3[i] = omegapredpointsm3_0
            print(i) #indicates that this function has started
        else:
            if i<79: #uses constant m for the lower part and changing m for the higher part
                (ams_list3[i], arl_predpoints_array3[i], omegapredpointsM_array3[i], 
                 omegapredpointsm_array3[i], alert3[i])  \
                = Minsep_full3(M_b, q_range1[i]*qb, q_range1[i]*R, q_range1[i]*dm_a, dM_b, 
                               arl_predpoints_array3_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert3[i] == 1 and phase4 == 1:
                    (ams_list3_prem[i], arl_list3_prem[i], arl_pred_list3_prem[i], arl_predpoints_array3_prem[i], 
                     omegapredpointsM_array3_prem[i], omegapredpointsm_array3_prem[i]) \
                    = Minsep_full4(M3_a, q_range1[i]*q3_a, R, q_range1[i]*dm_a, dM3_a, ams_list3[i], end_time, h)
            else:
                (ams_list3[i], arl_predpoints_array3[i], omegapredpointsM_array3[i], 
                 omegapredpointsm_array3[i], alert3[i]) \
                = Minsep_full3(M_b, q_range1[i]*q3_b, q_range1[i]*R, q_range1[i]*dm3_b, dM_b, 
                               arl_predpoints_array3_prem[i], phase4, end_time, h)
                
                #adjusts primary separation to match with secondary separation if the latter was adjusted
                if alert3[i] == 1 and phase4 == 1:
                    (ams_list3_prem[i], arl_list3_prem[i], arl_pred_list3_prem[i], arl_predpoints_array3_prem[i], 
                     omegapredpointsM_array3_prem[i], omegapredpointsm_array3_prem[i]) \
                    = Minsep_full4(M3_a, q_range1[i]*q3_a, R, q_range1[i]*dm_a, dM3_a, ams_list3[i], end_time, h)
            if i%10 == 0: #prints every 10th i, as a means of checking progress
                print(i)
                
    return (ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
            omegapredpointsM_array3, omegapredpointsm_array3)



#Function that overwrites primary and secondary evolution for large q when the primary and secondary are evolving at
#the same time
def Overwrite_Intermediate(ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, 
                      arl_predpoints_array3_prem, omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, 
                      ams_list3, arl_list3, 
                      arl_pred_list3, arl_predpoints_array3, omegapredpointsM_array3, omegapredpointsm_array3,
                      R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                      dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    for i in range(size1):
        if i > size1 - 4: #only overwrites large q
            (ams_list3[i], arl_list3[i], arl_pred_list3[i], arl_predpoints_array3[i], omegapredpointsM_array3[i], 
                 omegapredpointsm_array3[i]) \
                = Minsep_full(M3_a, q_range1[i]*qb, R, 
                                q_range1[i]*dm3_b, dM3_a, end_time, h)
            #primary and secondary separation are the same here
            ams_list3_prem[i] = np.copy(ams_list3[i])
            arl_list3_prem[i] = np.copy(arl_list3[i])
            arl_pred_list3_prem[i] = np.copy(arl_pred_list3[i])
            arl_predpoints_array3_prem[i] = np.copy(arl_predpoints_array3[i])
            omegapredpointsM_array3_prem[i] = np.copy(omegapredpointsM_array3[i])
            omegapredpointsm_array3_prem[i] = np.copy(omegapredpointsm_array3[i])
        
            
        
#Primary Evolution, allows for varying radius and mass loss
def Primary_Intermediate_Custom(R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                         dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                         q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #size of arrays
    size3 = len(R)
    size4 = len(M1_a[:,0])
    
    #arrays to store data
    ams_list3 = np.zeros((size1, size3, size4))
    arl_list3 = np.zeros((size1, size3, size4))
    arl_pred_list3 = np.zeros((size1, size3, size4))
    
    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                #for the 1st run, build some lists from scratch so their size can be determined and used in the future
                if i == 0 and j == 0 and k == 0:
                    (ams_list3[i,j,k], arl_list3[i,j,k], arl_pred_list3[i,j,k], arl_predpoints3_0, 
                     omegapredpointsM3_0, omegapredpointsm3_0) \
                    = Minsep_full2(M3_a[k], q_range1[i]*q3_a[k], R[j], q_range1[i]*dm_a[k], dM3_a[k], end_time, h)
                    size2 = len(arl_predpoints3_0)
                    
                    #since the data is already a list, 2d arrays of the appropriate size are created for storing data
                    arl_predpoints_array3 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsM_array3 = np.zeros((size1, size3, size4, size2))
                    omegapredpointsm_array3 = np.zeros((size1, size3, size4, size2))
                    
                    #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
                    arl_predpoints_array3[i,j,k] = arl_predpoints3_0
                    omegapredpointsM_array3[i,j,k] = omegapredpointsM3_0
                    omegapredpointsm_array3[i,j,k] = omegapredpointsm3_0
                    print(i, j, k) #indicates that this function has started
                else:
                    (ams_list3[i,j,k], arl_list3[i,j,k], arl_pred_list3[i,j,k], arl_predpoints_array3[i,j,k], 
                     omegapredpointsM_array3[i,j,k], omegapredpointsm_array3[i,j,k]) \
                    = Minsep_full2(M3_a[k], q_range1[i]*q3_a[k], R[j], q_range1[i]*dm_a[k], dM3_a[k], end_time, h)
                    if i%2 == 0 and j%2 == 0 and k%2 == 0: #prints every even entry, as a means of checking progress
                        print(i, j, k)
    
    #giving these values their own name so that the starting one can be reused later
    ams_list3_prem = np.copy(ams_list3)
    arl_list3_prem = np.copy(arl_list3)
    arl_pred_list3_prem = np.copy(arl_pred_list3)
    arl_predpoints_array3_prem = np.copy(arl_predpoints_array3)
    omegapredpointsM_array3_prem = np.copy(omegapredpointsM_array3)
    omegapredpointsm_array3_prem = np.copy(omegapredpointsm_array3)
    
    return (ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
            omegapredpointsM_array3_prem, omegapredpointsm_array3_prem)

            

#Binding energy only evolution for the primary     
def Primary_Intermediate_Binding(ams_list3_prem, 
                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                 q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #array to store data
    ams_binding3 = np.zeros(size1)
    
    for i in range(size1):
        if i == 0:
            ams_binding3[i], ams_points_binding3_0 = binding_energy(ams_list3_prem[i], M3_a, q_range1[i]*q3_a, 
                        R, q_range1[i]*dm_a, dM3_a, end_time, h)
            size2 = len(ams_points_binding3_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            ams_points_binding3 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            ams_points_binding3[i] = ams_points_binding3_0
        else:
            ams_binding3[i], ams_points_binding3[i] = binding_energy(ams_list3_prem[i], M3_a, q_range1[i]*q3_a, 
                        R, q_range1[i]*dm_a, dM3_a, end_time, h)
    
    #giving these values their own name so that the starting one can be reused later
    ams_binding3_prem = np.copy(ams_binding3)
    ams_points_binding3_prem = np.copy(ams_points_binding3)
    
    return ams_binding3_prem, ams_points_binding3_prem
  


#Binding energy only evolution for the primary and secondary
def Secondary_Intermediate_Binding(ams_binding3_prem, 
                                   R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                                   dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                   q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #array to store data
    ams_binding3 = np.zeros(size1)

    for i in range(size1):
        if i == 0:
            ams_binding3[i], ams_points_binding3_0 = binding_energy(ams_binding3_prem[i], M_b, q_range1[i]*qb, 
                        q_range1[i]*R, q_range1[i]*dm_a, dM_b, end_time, h)
            size2 = len(ams_points_binding3_0)
            
            #since the data is already a list, 2d arrays of the appropriate size are created for storing data
            ams_points_binding3 = np.zeros((size1, size2))
            
            #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
            ams_points_binding3[i] = ams_points_binding3_0
        else:
            if i<79: #uses constant m for the lower part and changing m for the higher part
                ams_binding3[i], ams_points_binding3[i] = binding_energy(ams_binding3_prem[i], M_b, q_range1[i]*qb, 
                            q_range1[i]*R, q_range1[i]*dm_a, dM_b, end_time, h)
            else:
                ams_binding3[i], ams_points_binding3[i] = binding_energy(ams_binding3_prem[i], M_b, q_range1[i]*q3_b, 
                            q_range1[i]*R, q_range1[i]*dm3_b, dM_b, end_time, h)
                
    return ams_binding3, ams_points_binding3



#Function that overwrites primary and secondary Binding energy for large q when the primary and secondary are evolving 
#at the same time
def Overwrite_Intermediate_Binding(ams_list3_prem, ams_list3, ams_binding3_prem, ams_points_binding3_prem, 
                                   ams_binding3, ams_points_binding3, R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, 
                                   dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, dM_b, dm1_b, dm2_b, dm3_b, 
                                   q_range1, size1, q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):       
    for i in range(size1):
        if i > size1 - 4: #only overwrites large q
            ams_binding3[i], ams_points_binding3[i] = binding_energy(ams_list3_prem[i], M3_a, q_range1[i]*qb, 
                        R, q_range1[i]*dm3_b, dM3_a, end_time, h)
    
        #primary and secondary separation are the same here    
        ams_binding3_prem[i] = np.copy(ams_binding3[i])
        ams_points_binding3_prem[i] = np.copy(ams_points_binding3[i])
    


#Binding energy only evolution for the primary, allows for varying radius and mass loss   
def Primary_Intermediate_Binding_Custom(ams_list3_prem, 
                                 R, M_a, M1_a, M2_a, M3_a, m_a, dM1_a, dM2_a, dM3_a, dm_a, M_b, m1_b, m2_b, m3_b, 
                                 dM_b, dm1_b, dm2_b, dm3_b, q_range1, size1, 
                                 q1_a, q2_a, q3_a, q1_b, q2_b, q3_b, qb, end_time, h):
    #size of arrays
    size3 = len(R)
    size4 = len(M1_a[:,0])
    #array to store data
    ams_binding3 = np.zeros((size1, size3, size4))
    
    for i in range(size1):
        for j in range(size3):
            for k in range(size4):
                if i == 0 and j == 0 and k == 0:
                    ams_binding3[i,j,k], ams_points_binding3_0 = binding_energy(ams_list3_prem[i,j,k], M3_a[k], 
                                                                                q_range1[i]*q3_a[k], 
                                R[j], q_range1[i]*dm_a[k], dM3_a[k], end_time, h)
                    size2 = len(ams_points_binding3_0)
                    
                    #since the data is already a list, 2d arrays of the appropriate size are created for storing data
                    ams_points_binding3 = np.zeros((size1, size3, size4, size2))
                    
                    #filling in the first row, which was already calculated. For subsequent i, rows can be stored directly
                    ams_points_binding3[i,j,k] = ams_points_binding3_0
                else:
                    ams_binding3[i,j,k], ams_points_binding3[i,j,k] = binding_energy(ams_list3_prem[i,j,k], M3_a[k],
                                                                                     q_range1[i]*q3_a[k], 
                                R[j], q_range1[i]*dm_a[k], dM3_a[k], end_time, h)
    
    #giving these values their own name so that the starting one can be reused later
    ams_binding3_prem = np.copy(ams_binding3)
    ams_points_binding3_prem = np.copy(ams_points_binding3)
    
    return ams_binding3_prem, ams_points_binding3_prem



'''
Functions for converting separation to period
'''
#Function of kepler's 3rd law
def period_time(sep, M, m):
    period = ((4*(np.pi**2)*(sep**3))/(G*(M + m)))**0.5
    return period



#Function that converts separation to period
def Period_Convert(period1, period2, period3, sep1, sep2, sep3, q_range1, size1):
    for i in range(size1):
        if i<79: #uses constant m for the lower half and changing m for the higher half
            period1[i] = period_time(sep1[i], 0.8, q_range1[i])
        else:
            period1[i] = period_time(sep1[i], 0.8, q_range1[i]*0.8)
            
    for i in range(size1):
        if i<79: #uses constant m for the lower half and changing m for the higher half
            period2[i] = period_time(sep2[i], 0.8, q_range1[i])
        else:
            period2[i] = period_time(sep2[i], 0.8, q_range1[i]*0.8)
            
    for i in range(size1):
        if i<79: #uses constant m for the lower half and changing m for the higher half
            period3[i] = period_time(sep3[i], 0.8, q_range1[i])
        else:
            period3[i] = period_time(sep3[i], 0.8, q_range1[i]*0.8)
  
    

'''
Saving and Loading Functions
'''
#Saving data for phases that have only the primary evolving
def Save1(folder, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem,
         ams_binding1_prem, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
         arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem,
         ams_binding2_prem, ams_points_binding2_prem, ams_list3_prem, arl_list3_prem, 
         arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
         omegapredpointsm_array3_prem, ams_binding3_prem, ams_points_binding3_prem):
    np.save(os.path.join(folder, "ams_list1_prem"), ams_list1_prem)
    np.save(os.path.join(folder, "arl_list1_prem"), arl_list1_prem)
    np.save(os.path.join(folder, "arl_pred_list1_prem"), arl_pred_list1_prem)
    np.save(os.path.join(folder, "arl_predpoints_array1_prem"), arl_predpoints_array1_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array1_prem"), omegapredpointsM_array1_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array1_prem"), omegapredpointsm_array1_prem)
    np.save(os.path.join(folder, "e_list1_prem"), e_list1_prem)
    
    np.save(os.path.join(folder, "ams_binding1_prem"), ams_binding1_prem)
    
    np.save(os.path.join(folder, "ams_list2_prem"), ams_list2_prem)
    np.save(os.path.join(folder, "arl_list2_prem"), arl_list2_prem)
    np.save(os.path.join(folder, "arl_pred_list2_prem"), arl_pred_list2_prem)
    np.save(os.path.join(folder, "arl_predpoints_array2_prem"), arl_predpoints_array2_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array2_prem"), omegapredpointsM_array2_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array2_prem"), omegapredpointsm_array2_prem)
    
    np.save(os.path.join(folder, "ams_binding2_prem"), ams_binding2_prem)
    np.save(os.path.join(folder, "ams_points_binding2_prem"), ams_points_binding2_prem)
    
    np.save(os.path.join(folder, "ams_list3_prem"), ams_list3_prem)
    np.save(os.path.join(folder, "arl_list3_prem"), arl_list3_prem)
    np.save(os.path.join(folder, "arl_pred_list3_prem"), arl_pred_list3_prem)
    np.save(os.path.join(folder, "arl_predpoints_array3_prem"), arl_predpoints_array3_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array3_prem"), omegapredpointsM_array3_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array3_prem"), omegapredpointsm_array3_prem)
    
    np.save(os.path.join(folder, "ams_binding3_prem"), ams_binding3_prem)
    np.save(os.path.join(folder, "ams_points_binding3_prem"), ams_points_binding3_prem)
    
    

#Saving data for phases that have the primary and secondary evolving
def Save2(folder, ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
          omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
          arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
          ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
          arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
          arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
          ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
          arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
          omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
          omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, ams_points_binding3_prem, 
          ams_binding3, ams_points_binding3):
    np.save(os.path.join(folder, "ams_list1_prem"), ams_list1_prem)
    np.save(os.path.join(folder, "arl_list1_prem"), arl_list1_prem)
    np.save(os.path.join(folder, "arl_pred_list1_prem"), arl_pred_list1_prem)
    np.save(os.path.join(folder, "arl_predpoints_array1_prem"), arl_predpoints_array1_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array1_prem"), omegapredpointsM_array1_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array1_prem"), omegapredpointsm_array1_prem)
    np.save(os.path.join(folder, "e_list1_prem"), e_list1_prem)
    
    np.save(os.path.join(folder, "ams_list1"), ams_list1)
    np.save(os.path.join(folder, "arl_list1"), arl_list1)
    np.save(os.path.join(folder, "arl_pred_list1"), arl_pred_list1)
    np.save(os.path.join(folder, "arl_predpoints_array1"), arl_predpoints_array1)
    np.save(os.path.join(folder, "omegapredpointsM_array1"), omegapredpointsM_array1)
    np.save(os.path.join(folder, "omegapredpointsm_array1"), omegapredpointsm_array1)
    np.save(os.path.join(folder, "e_list1"), e_list1)
    
    np.save(os.path.join(folder, "ams_binding1_prem"), ams_binding1_prem)
    np.save(os.path.join(folder, "ams_binding1"), ams_binding1)
    
    np.save(os.path.join(folder, "ams_list2_prem"), ams_list2_prem)
    np.save(os.path.join(folder, "arl_list2_prem"), arl_list2_prem)
    np.save(os.path.join(folder, "arl_pred_list2_prem"), arl_pred_list2_prem)
    np.save(os.path.join(folder, "arl_predpoints_array2_prem"), arl_predpoints_array2_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array2_prem"), omegapredpointsM_array2_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array2_prem"), omegapredpointsm_array2_prem)
    
    np.save(os.path.join(folder, "ams_list2"), ams_list2)
    np.save(os.path.join(folder, "arl_list2"), arl_list2)
    np.save(os.path.join(folder, "arl_pred_list2"), arl_pred_list2)
    np.save(os.path.join(folder, "arl_predpoints_array2"), arl_predpoints_array2)
    np.save(os.path.join(folder, "omegapredpointsM_array2"), omegapredpointsM_array2)
    np.save(os.path.join(folder, "omegapredpointsm_array2"), omegapredpointsm_array2)
    
    np.save(os.path.join(folder, "ams_binding2_prem"), ams_binding2_prem)
    np.save(os.path.join(folder, "ams_points_binding2_prem"), ams_points_binding2_prem)
    np.save(os.path.join(folder, "ams_binding2"), ams_binding2)
    np.save(os.path.join(folder, "ams_points_binding2"), ams_points_binding2)
    
    np.save(os.path.join(folder, "ams_list3_prem"), ams_list3_prem)
    np.save(os.path.join(folder, "arl_list3_prem"), arl_list3_prem)
    np.save(os.path.join(folder, "arl_pred_list3_prem"), arl_pred_list3_prem)
    np.save(os.path.join(folder, "arl_predpoints_array3_prem"), arl_predpoints_array3_prem)
    np.save(os.path.join(folder, "omegapredpointsM_array3_prem"), omegapredpointsM_array3_prem)
    np.save(os.path.join(folder, "omegapredpointsm_array3_prem"), omegapredpointsm_array3_prem)
    
    np.save(os.path.join(folder, "ams_list3"), ams_list3)
    np.save(os.path.join(folder, "arl_list3"), arl_list3)
    np.save(os.path.join(folder, "arl_pred_list3"), arl_pred_list3)
    np.save(os.path.join(folder, "arl_predpoints_array3"), arl_predpoints_array3)
    np.save(os.path.join(folder, "omegapredpointsM_array3"), omegapredpointsM_array3)
    np.save(os.path.join(folder, "omegapredpointsm_array3"), omegapredpointsm_array3)
    
    np.save(os.path.join(folder, "ams_binding3_prem"), ams_binding3_prem)
    np.save(os.path.join(folder, "ams_points_binding3_prem"), ams_points_binding3_prem)
    np.save(os.path.join(folder, "ams_binding3"), ams_binding3)
    np.save(os.path.join(folder, "ams_points_binding3"), ams_points_binding3)
    
    

#loading data for phases that have only the primary evolving
def Load1(folder):
    ams_list1_prem = np.load(os.path.join(folder, "ams_list1_prem.npy"))
    arl_list1_prem = np.load(os.path.join(folder, "arl_list1_prem.npy"))
    arl_pred_list1_prem = np.load(os.path.join(folder, "arl_pred_list1_prem.npy"))
    arl_predpoints_array1_prem = np.load(os.path.join(folder, "arl_predpoints_array1_prem.npy"))
    omegapredpointsM_array1_prem = np.load(os.path.join(folder, "omegapredpointsM_array1_prem.npy"))
    omegapredpointsm_array1_prem = np.load(os.path.join(folder, "omegapredpointsm_array1_prem.npy"))
    e_list1_prem = np.load(os.path.join(folder, "e_list1_prem.npy"))
    
    ams_binding1_prem = np.load(os.path.join(folder, "ams_binding1_prem.npy"))
    
    ams_list2_prem = np.load(os.path.join(folder, "ams_list2_prem.npy"))
    arl_list2_prem = np.load(os.path.join(folder, "arl_list2_prem.npy"))
    arl_pred_list2_prem = np.load(os.path.join(folder, "arl_pred_list2_prem.npy"))
    arl_predpoints_array2_prem = np.load(os.path.join(folder, "arl_predpoints_array2_prem.npy"))
    omegapredpointsM_array2_prem = np.load(os.path.join(folder, "omegapredpointsM_array2_prem.npy"))
    omegapredpointsm_array2_prem = np.load(os.path.join(folder, "omegapredpointsm_array2_prem.npy"))
    
    ams_binding2_prem = np.load(os.path.join(folder, "ams_binding2_prem.npy"))
    ams_points_binding2_prem = np.load(os.path.join(folder, "ams_points_binding2_prem.npy"))
    
    ams_list3_prem = np.load(os.path.join(folder, "ams_list3_prem.npy"))
    arl_list3_prem = np.load(os.path.join(folder, "arl_list3_prem.npy"))
    arl_pred_list3_prem = np.load(os.path.join(folder, "arl_pred_list3_prem.npy"))
    arl_predpoints_array3_prem = np.load(os.path.join(folder, "arl_predpoints_array3_prem.npy"))
    omegapredpointsM_array3_prem = np.load(os.path.join(folder, "omegapredpointsM_array3_prem.npy"))
    omegapredpointsm_array3_prem = np.load(os.path.join(folder, "omegapredpointsm_array3_prem.npy"))
    
    ams_binding3_prem = np.load(os.path.join(folder, "ams_binding3_prem.npy"))
    ams_points_binding3_prem = np.load(os.path.join(folder, "ams_points_binding3_prem.npy"))
    return (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
            omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_binding1_prem, 
            ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, arl_predpoints_array2_prem, 
            omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_binding2_prem, ams_points_binding2_prem, 
            ams_list3_prem, arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, 
            omegapredpointsM_array3_prem, omegapredpointsm_array3_prem, ams_binding3_prem, ams_points_binding3_prem)
    
    

#Loading data for phases that have the primary and secondary evolving
def Load2(folder):
    ams_list1_prem = np.load(os.path.join(folder, "ams_list1_prem.npy"))
    arl_list1_prem = np.load(os.path.join(folder, "arl_list1_prem.npy"))
    arl_pred_list1_prem = np.load(os.path.join(folder, "arl_pred_list1_prem.npy"))
    arl_predpoints_array1_prem = np.load(os.path.join(folder, "arl_predpoints_array1_prem.npy"))
    omegapredpointsM_array1_prem = np.load(os.path.join(folder, "omegapredpointsM_array1_prem.npy"))
    omegapredpointsm_array1_prem = np.load(os.path.join(folder, "omegapredpointsm_array1_prem.npy"))
    e_list1_prem = np.load(os.path.join(folder, "e_list1_prem.npy"))
    
    ams_list1 = np.load(os.path.join(folder, "ams_list1.npy"))
    arl_list1 = np.load(os.path.join(folder, "arl_list1.npy"))
    arl_pred_list1 = np.load(os.path.join(folder, "arl_pred_list1.npy"))
    arl_predpoints_array1 = np.load(os.path.join(folder, "arl_predpoints_array1.npy"))
    omegapredpointsM_array1 = np.load(os.path.join(folder, "omegapredpointsM_array1.npy"))
    omegapredpointsm_array1 = np.load(os.path.join(folder, "omegapredpointsm_array1.npy"))
    e_list1 = np.load(os.path.join(folder, "e_list1.npy"))
    
    ams_binding1_prem = np.load(os.path.join(folder, "ams_binding1_prem.npy"))
    ams_binding1 = np.load(os.path.join(folder, "ams_binding1.npy"))
    
    ams_list2_prem = np.load(os.path.join(folder, "ams_list2_prem.npy"))
    arl_list2_prem = np.load(os.path.join(folder, "arl_list2_prem.npy"))
    arl_pred_list2_prem = np.load(os.path.join(folder, "arl_pred_list2_prem.npy"))
    arl_predpoints_array2_prem = np.load(os.path.join(folder, "arl_predpoints_array2_prem.npy"))
    omegapredpointsM_array2_prem = np.load(os.path.join(folder, "omegapredpointsM_array2_prem.npy"))
    omegapredpointsm_array2_prem = np.load(os.path.join(folder, "omegapredpointsm_array2_prem.npy"))
    
    ams_list2 = np.load(os.path.join(folder, "ams_list2.npy"))
    arl_list2 = np.load(os.path.join(folder, "arl_list2.npy"))
    arl_pred_list2 = np.load(os.path.join(folder, "arl_pred_list2.npy"))
    arl_predpoints_array2 = np.load(os.path.join(folder, "arl_predpoints_array2.npy"))
    omegapredpointsM_array2 = np.load(os.path.join(folder, "omegapredpointsM_array2.npy"))
    omegapredpointsm_array2 = np.load(os.path.join(folder, "omegapredpointsm_array2.npy"))
    
    ams_binding2_prem = np.load(os.path.join(folder, "ams_binding2_prem.npy"))
    ams_points_binding2_prem = np.load(os.path.join(folder, "ams_points_binding2_prem.npy"))
    ams_binding2 = np.load(os.path.join(folder, "ams_binding2.npy"))
    ams_points_binding2 = np.load(os.path.join(folder, "ams_points_binding2.npy"))
    
    ams_list3_prem = np.load(os.path.join(folder, "ams_list3_prem.npy"))
    arl_list3_prem = np.load(os.path.join(folder, "arl_list3_prem.npy"))
    arl_pred_list3_prem = np.load(os.path.join(folder, "arl_pred_list3_prem.npy"))
    arl_predpoints_array3_prem = np.load(os.path.join(folder, "arl_predpoints_array3_prem.npy"))
    omegapredpointsM_array3_prem = np.load(os.path.join(folder, "omegapredpointsM_array3_prem.npy"))
    omegapredpointsm_array3_prem = np.load(os.path.join(folder, "omegapredpointsm_array3_prem.npy"))
    
    ams_list3 = np.load(os.path.join(folder, "ams_list3.npy"))
    arl_list3 = np.load(os.path.join(folder, "arl_list3.npy"))
    arl_pred_list3 = np.load(os.path.join(folder, "arl_pred_list3.npy"))
    arl_predpoints_array3 = np.load(os.path.join(folder, "arl_predpoints_array3.npy"))
    omegapredpointsM_array3 = np.load(os.path.join(folder, "omegapredpointsM_array3.npy"))
    omegapredpointsm_array3 = np.load(os.path.join(folder, "omegapredpointsm_array3.npy"))
    
    ams_binding3_prem = np.load(os.path.join(folder, "ams_binding3_prem.npy"))
    ams_points_binding3_prem = np.load(os.path.join(folder, "ams_points_binding3_prem.npy"))
    ams_binding3 = np.load(os.path.join(folder, "ams_binding3.npy"))
    ams_points_binding3 = np.load(os.path.join(folder, "ams_points_binding3.npy"))
    return (ams_list1_prem, arl_list1_prem, arl_pred_list1_prem, arl_predpoints_array1_prem, 
            omegapredpointsM_array1_prem, omegapredpointsm_array1_prem, e_list1_prem, ams_list1, arl_list1, 
            arl_pred_list1, arl_predpoints_array1, omegapredpointsM_array1, omegapredpointsm_array1, e_list1, 
            ams_binding1_prem, ams_binding1, ams_list2_prem, arl_list2_prem, arl_pred_list2_prem, 
            arl_predpoints_array2_prem, omegapredpointsM_array2_prem, omegapredpointsm_array2_prem, ams_list2, 
            arl_list2, arl_pred_list2, arl_predpoints_array2, omegapredpointsM_array2, omegapredpointsm_array2, 
            ams_binding2_prem, ams_points_binding2_prem, ams_binding2, ams_points_binding2, ams_list3_prem, 
            arl_list3_prem, arl_pred_list3_prem, arl_predpoints_array3_prem, omegapredpointsM_array3_prem, 
            omegapredpointsm_array3_prem, ams_list3, arl_list3, arl_pred_list3, arl_predpoints_array3, 
            omegapredpointsM_array3, omegapredpointsm_array3, ams_binding3_prem, 
            ams_points_binding3_prem, ams_binding3, ams_points_binding3)
